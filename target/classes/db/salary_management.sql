/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50713
 Source Host           : localhost:3307
 Source Schema         : salary_management

 Target Server Type    : MySQL
 Target Server Version : 50713
 File Encoding         : 65001

 Date: 07/11/2023 17:50:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(10) NOT NULL COMMENT '主键',
  `username` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '密码',
  `phone` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '手机号（忘记密码时使用）',
  `code` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '验证码（模拟发送验证码）',
  `del_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标志',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'user', '123456', '15280890268', '123456', 0);
INSERT INTO `user` VALUES (1721730314726899714, 'bejav', '123456', '15280890222', '123455', 0);
INSERT INTO `user` VALUES (1721782153107304449, 'bejav2', '123456', '13055207526', '123456', 0);

-- ----------------------------
-- Table structure for pay_roll
-- ----------------------------
DROP TABLE IF EXISTS `pay_roll`;
CREATE TABLE `pay_roll`  (
  `id` bigint(10) NOT NULL COMMENT '主键',
  `staff_id` bigint(10) NULL DEFAULT NULL COMMENT '员工档案表外键',
  `staff_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '员工名称',
  `contract_id` bigint(10) NULL DEFAULT NULL COMMENT '合同表外键',
  `basic_salary` decimal(10, 2) NULL DEFAULT NULL COMMENT '基本工资（从合同表中获取合同报酬字段值）',
  `bonus` decimal(10, 2) NULL DEFAULT NULL COMMENT '奖金',
  `fine` decimal(10, 2) NULL DEFAULT NULL COMMENT '罚款',
  `individual_income_tax` decimal(10, 2) NULL DEFAULT NULL COMMENT '个人所得税',
  `endownment_insurance` decimal(10, 2) NULL DEFAULT NULL COMMENT '个人养老保险',
  `medical_insurance` decimal(10, 2) NULL DEFAULT NULL COMMENT '个人医疗保险',
  `unemployment_insurance` decimal(10, 2) NULL DEFAULT NULL COMMENT '个人失业保险',
  `maternity_insurance` decimal(10, 2) NULL DEFAULT NULL COMMENT '个人生育保险',
  `employment_injury_insurance` decimal(10, 2) NULL DEFAULT NULL COMMENT '个人工伤保险',
  `accumulation_fund` decimal(10, 2) NULL DEFAULT NULL COMMENT '个人公积金',
  `reward_id` bigint(10) NULL DEFAULT NULL COMMENT '奖惩表外键',
  `work_hour_id` bigint(10) NULL DEFAULT NULL COMMENT '工时表外键',
  `salary_level_id` bigint(10) NULL DEFAULT NULL COMMENT '薪资等级表外键',
  `salary_level` tinyint(1) NULL DEFAULT NULL COMMENT '薪资等级（1-初级，2-中级，3-高级）',
  `actual_work_time` datetime(0) NULL DEFAULT NULL COMMENT '实际工作时间',
  `payable_salary` decimal(10, 2) NULL DEFAULT NULL COMMENT '应发工资',
  `net_salary` decimal(10, 2) NULL DEFAULT NULL COMMENT '实发工资',
  `date` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '结算日期（按月结算，yyyy-MM）',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '删除标志',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '员工薪资单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pay_roll
-- ----------------------------
INSERT INTO `pay_roll` VALUES (1721805722059116546, 1721805722004590593, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-11-07 16:24:08', NULL, '2023-11-07 16:24:08', NULL, 0);

-- ----------------------------
-- Table structure for reward_and_punishment
-- ----------------------------
DROP TABLE IF EXISTS `reward_and_punishment`;
CREATE TABLE `reward_and_punishment`  (
  `id` bigint(10) NOT NULL COMMENT '主键',
  `staff_id` bigint(10) NULL DEFAULT NULL COMMENT '员工档案表外键',
  `reward_reason` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '奖励原因',
  `reward_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '奖金',
  `punish_reason` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '惩罚原因',
  `punish_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '罚款',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `create_by` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `update_by` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '修改人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '删除标志',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '员工奖惩情况表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of reward_and_punishment
-- ----------------------------
INSERT INTO `reward_and_punishment` VALUES (1721805722046533634, 1721805722004590593, NULL, NULL, NULL, NULL, '2023-11-07 16:24:08', NULL, '2023-11-07 16:24:08', NULL, 0);

-- ----------------------------
-- Table structure for salary_level
-- ----------------------------
DROP TABLE IF EXISTS `salary_level`;
CREATE TABLE `salary_level`  (
  `id` bigint(10) NOT NULL COMMENT '主键',
  `start_salary_range` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '薪资起始范围',
  `end_salary_range` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '薪资截止范围',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `create_by` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `update_by` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '修改人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '删除标志',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '薪资等级表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of salary_level
-- ----------------------------

-- ----------------------------
-- Table structure for staff_contract
-- ----------------------------
DROP TABLE IF EXISTS `staff_contract`;
CREATE TABLE `staff_contract`  (
  `id` bigint(10) NOT NULL COMMENT '主键',
  `staff_id` bigint(10) NULL DEFAULT NULL COMMENT '员工档案表外键',
  `employer_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用人单位名称',
  `employer_address` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用人单位地址',
  `employer_legal_person` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用人单位法人',
  `worker_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '劳动者名称',
  `worker_address` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '劳动者住址',
  `worker_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '劳动者身份证号',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '合同起始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '合同截止时间',
  `remuneration` decimal(10, 2) NULL DEFAULT NULL COMMENT '合同报酬',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `create_by` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `update_by` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '修改人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '删除标志',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '员工合同表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of staff_contract
-- ----------------------------
INSERT INTO `staff_contract` VALUES (1721805722017173505, 1721805722004590593, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-11-07 16:24:08', NULL, '2023-11-07 16:24:08', NULL, 0);

-- ----------------------------
-- Table structure for staff_file
-- ----------------------------
DROP TABLE IF EXISTS `staff_file`;
CREATE TABLE `staff_file`  (
  `id` bigint(10) NOT NULL COMMENT '员工编号（主键）',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `sex` tinyint(1) NULL DEFAULT NULL COMMENT '性别（0-男，1-女）',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `birth` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '出生日期',
  `phone` varchar(16) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '手机号',
  `address` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '详细住址',
  `email` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `education` tinyint(1) NULL DEFAULT NULL COMMENT '学历（1-大专，2-本科，3-硕士，4-博士，5-初中，6-高中）',
  `marriage_status` tinyint(1) NULL DEFAULT NULL COMMENT '婚姻情况（0-未婚，1-已婚，2-离婚）',
  `personal_experience` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '个人经历',
  `nation` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '民族',
  `contract_id` bigint(10) NULL DEFAULT NULL COMMENT '合同表外键',
  `salary_level_id` bigint(10) NULL DEFAULT NULL COMMENT '薪资等级表外键',
  `status` tinyint(1) NULL DEFAULT 0 COMMENT '员工状态（0-在职，1-离职）',
  `salary_id` bigint(10) NULL DEFAULT NULL COMMENT '薪资表外键',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '删除标志',
  `entry_time` datetime(0) NULL DEFAULT NULL COMMENT '入职时间',
  `reward_id` bigint(10) NULL DEFAULT NULL COMMENT '奖惩表外键',
  `hour_id` bigint(10) NULL DEFAULT NULL COMMENT '工时表外键',
  `province_id` int(11) NULL DEFAULT NULL COMMENT '省编号',
  `city_id` int(11) NULL DEFAULT NULL COMMENT '市编号',
  `area_id` int(11) NULL DEFAULT NULL COMMENT '区编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '员工档案表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of staff_file
-- ----------------------------
INSERT INTO `staff_file` VALUES (1721800824689111041, '小苏', '350526200010070018', 0, 23, '2000-10-7', '15280890269', '福建省厦门市集美区', '2823266419@qq.com', 2, 0, 'xxx', '汉族', NULL, NULL, 0, NULL, '2023-11-07 16:04:41', NULL, NULL, NULL, 0, '2023-11-07 16:04:41', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `staff_file` VALUES (1721801123780734978, '小张', '350526200010070017', 0, 23, '2000-10-7', '15280890262', '福建省厦门市集美区', '2823266419@qq.com', 2, 0, 'xxx', '汉族', NULL, NULL, 0, NULL, '2023-11-07 16:05:52', NULL, NULL, NULL, 0, '2023-11-07 16:05:52', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `staff_file` VALUES (1721805722004590593, '小林', '350526200010070051', 0, 23, '2000-10-7', '15280890205', '福建省厦门市集美区', '2823266419@qq.com', 2, 0, 'xxx', '汉族', 1721805722017173505, NULL, 0, 1721805722059116546, '2023-11-07 16:24:08', NULL, '2023-11-07 16:24:08', NULL, 0, '2023-11-07 16:24:08', 1721805722046533634, 1721805722029756417, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for work_hour_record
-- ----------------------------
DROP TABLE IF EXISTS `work_hour_record`;
CREATE TABLE `work_hour_record`  (
  `id` bigint(10) NOT NULL COMMENT '主键',
  `staff_id` bigint(10) NULL DEFAULT NULL COMMENT '员工档案表外键',
  `work_hours` int(11) NULL DEFAULT NULL COMMENT '工时',
  `date` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '工时计算日期（yyyy-MM）',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `create_by` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `update_by` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '修改人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '删除标志',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '工时记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of work_hour_record
-- ----------------------------
INSERT INTO `work_hour_record` VALUES (1721805722029756417, 1721805722004590593, NULL, NULL, '2023-11-07 16:24:08', NULL, '2023-11-07 16:24:08', NULL, 0);

SET FOREIGN_KEY_CHECKS = 1;
