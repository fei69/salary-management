package com.salary.management.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class Knife4jConfig {

    @Bean
    public Docket defaultApi2() {
        String groupName="2.X版本";
        Docket docket=new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .title("工资管理系统API接口文档 ")
                        .description("# API接口文档")
                        //.termsOfServiceUrl("http://yaomaoyang.com")
                        //.contact(new Contact("逆流星","http://yaomaoyang.com","361426201@qq.com"))
                        .version("2.0.5")
                        .build())
                //分组名称
                .groupName(groupName)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.salary.management"))
                .paths(PathSelectors.any())
                .build();
        return docket;
    }
}
