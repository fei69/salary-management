package com.salary.management.controller;

import com.salary.management.pojo.user.dto.ForgetPasswordDTO;
import com.salary.management.pojo.user.dto.RegisterUserDTO;
import com.salary.management.service.LoginService;
import com.salary.management.utils.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(tags = "用户接口")
@RestController
@RequestMapping("/admin")
public class LoginController extends BaseController{

    @Resource
    private LoginService loginService;

    @ApiOperation("登录")
    @GetMapping("/login")
    public ResponseResult login(@RequestParam String username,@RequestParam String password){
        return loginService.login(username, password);
    }

    @ApiOperation("修改密码")
    @GetMapping("/updatePassword")
    public ResponseResult updatePassword(@RequestParam Long id,@RequestParam String oldPassword,
                                         @RequestParam String newPassword){
        return loginService.updatePassword(id,oldPassword,newPassword);
    }

    @ApiOperation("注销")
    @GetMapping("/logout")
    public ResponseResult logout(){
        return loginService.logout();
    }

    @ApiOperation("注册")
    @PostMapping("/register")
    public ResponseResult register(@RequestBody @Validated RegisterUserDTO dto){
        return loginService.register(dto);
    }

    @ApiOperation("忘记密码")
    @PostMapping("/forgetPassword")
    public ResponseResult forgetPassword(@RequestBody ForgetPasswordDTO dto){
        return loginService.forgetPassword(dto);
    }
}
