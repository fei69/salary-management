/*
package com.salary.management.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.salary.management.pojo.contract.dto.AddStaffContractDTO;
import com.salary.management.pojo.contract.dto.UpdateStaffContractDTO;
import com.salary.management.pojo.contract.vo.SearchContractVO;
import com.salary.management.pojo.contract.dto.SearchContractDTO;
import com.salary.management.service.StaffContractService;
import com.salary.management.utils.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(tags = "员工合同接口")
@RestController
@RequestMapping("/staffContract")
public class StaffContractController {

    @Resource
    private StaffContractService staffContractService;

    @ApiOperation("新增合同（备用，因为创建员工档案的同时也会创建对应的合同，只要进行修改操作来补充信息即可）")
    @PostMapping("/addStaffContact")
    public ResponseResult addStaffContract(@RequestBody @Validated AddStaffContractDTO dto){
        return staffContractService.addStaffContract(dto);
    }

    @ApiOperation("修改合同（劳动报酬不传）")
    @PostMapping("/updateStaffContract")
    public ResponseResult updateStaffContract(@RequestBody @Validated UpdateStaffContractDTO dto){
        return staffContractService.updateStaffContract(dto);
    }

    @ApiOperation("分页查询合同")
    @PostMapping("/listStaffContract")
    public ResponseResult<IPage<SearchContractVO>> listStaffContract(@RequestBody SearchContractDTO dto){
        Page<SearchContractVO> page = new Page<>(dto.getPageNum(), dto.getPageSize());
        IPage<SearchContractVO> result = staffContractService.listStaffContract(dto, page);
        return ResponseResult.okResult(result);
    }

    @ApiOperation("根据id查询合同信息（用于修改界面调用数据）")
    @GetMapping("/getStaffContractById")
    public ResponseResult getStaffContractById(@RequestParam("id") Long id){
        return staffContractService.getStaffContractById(id);
    }

    @ApiOperation("删除合同")
    @DeleteMapping("/deleteStaffContract")
    public ResponseResult deleteStaffContract(@RequestParam("id") Long id){
        return staffContractService.deleteStaffContract(id);
    }

}
*/
