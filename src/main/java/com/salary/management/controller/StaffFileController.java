package com.salary.management.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.salary.management.pojo.file.dto.AddFileDTO;
import com.salary.management.pojo.file.dto.SearchFileDTO;
import com.salary.management.pojo.file.dto.UpdateFileDTO;
import com.salary.management.pojo.file.vo.SearchFileVO;
import com.salary.management.service.StaffFileService;
import com.salary.management.utils.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.IOException;

@RestController
@Api(tags = "员工档案接口")
@RequestMapping("/staffFile")
public class StaffFileController extends BaseController{

    @Resource
    private StaffFileService staffFileService;

    @ApiOperation("添加员工档案")
    @PostMapping("/addStaffFile")
    public ResponseResult addStaffFile(@RequestBody @Validated AddFileDTO dto){
        return staffFileService.addStaffFile(dto);
    }


    @ApiOperation("分页查询员工档案")
    @PostMapping("/listStaffFile")
    public ResponseResult<IPage<SearchFileVO>> listStaffFile(@RequestBody SearchFileDTO dto){
        Page<SearchFileVO> page = new Page<>(dto.getPageNum(),dto.getPageSize());
        IPage<SearchFileVO> result = staffFileService.listStaffFile(dto, page);
        return ResponseResult.okResult(result);
    }

    @ApiOperation("修改员工档案")
    @PostMapping("/updateStaffFile")
    public ResponseResult updateStaffFile(@RequestBody @Validated UpdateFileDTO dto){
        return staffFileService.updateStaffFile(dto);
    }

    @ApiOperation("删除员工档案")
    @DeleteMapping("/deleteStaffFile")
    public ResponseResult deleteStaffFile(Long id){
        return staffFileService.deleteStaffFile(id);
    }

    @ApiOperation("批量导出员工档案")
    @GetMapping("/exportStaffFile")
    public ResponseResult exportStaffFile() throws IOException{
        return staffFileService.exportStaffFile();
    }

}
