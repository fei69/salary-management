package com.salary.management.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.salary.management.pojo.composition.dto.AddCompositionDTO;
import com.salary.management.pojo.composition.dto.SearchCompositionDTO;
import com.salary.management.pojo.composition.dto.UpdateCompositionDTO;
import com.salary.management.pojo.composition.vo.SearchCompositionVO;
import com.salary.management.service.SalaryCompositionService;
import com.salary.management.utils.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(tags = "薪资组成接口")
@RestController
@RequestMapping("/salaryComposition")
public class SalaryCompositionController {

    @Resource
    private SalaryCompositionService salaryCompositionService;

    @ApiOperation("初始化薪资组成")
    @PostMapping("/addSalaryComposition")
    public ResponseResult addSalaryComposition(@RequestBody @Validated AddCompositionDTO dto){
        return salaryCompositionService.addSalaryComposition(dto);
    }

    @ApiOperation("修改薪资组成")
    @PostMapping("/updateSalaryComposition")
    public ResponseResult updateSalaryComposition(@RequestBody @Validated UpdateCompositionDTO dto){
        return salaryCompositionService.updateSalaryComposition(dto);
    }

    @ApiOperation("分页查询薪资组成情况")
    @PostMapping("/listSalaryComposition")
    public ResponseResult<IPage<SearchCompositionVO>> listSalaryComposition(@RequestBody SearchCompositionDTO dto){
        Page<Object> page = new Page<>(dto.getPageNum(), dto.getPageSize());
        IPage<SearchCompositionVO> result = salaryCompositionService.listSalaryComposition(dto, page);
        return ResponseResult.okResult(result);
    }

    @DeleteMapping("/deleteSalaryComposition")
    public ResponseResult deleteSalaryComposition(@RequestParam(value = "id") Long id){
        return salaryCompositionService.deleteSalaryComposition(id);
    }

}
