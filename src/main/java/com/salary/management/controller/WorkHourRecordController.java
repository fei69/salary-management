package com.salary.management.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.salary.management.pojo.workhour.dto.AddWorkHourDTO;
import com.salary.management.pojo.workhour.dto.SearchHourDTO;
import com.salary.management.pojo.workhour.dto.UpdateWorkHourDTO;
import com.salary.management.pojo.workhour.vo.SearchHourVO;
import com.salary.management.service.WorkHourRecordService;
import com.salary.management.utils.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(tags = "工时记录模块接口")
@RestController
@RequestMapping("/workHourRecord")
public class WorkHourRecordController {

    @Resource
    private WorkHourRecordService workHourRecordService;


    @ApiOperation("添加工时记录")
    @PostMapping("/addWorkHourRecord")
    public ResponseResult addWorkHourRecord(@RequestBody @Validated AddWorkHourDTO dto){
        return workHourRecordService.addWorkHourRecord(dto);
    }

    @ApiOperation("修改工时记录")
    @PostMapping("/updateWorkHourRecord")
    public ResponseResult updateWorkHourRecord(@RequestBody @Validated UpdateWorkHourDTO dto){
        return workHourRecordService.updateWorkHourRecord(dto);
    }

    @ApiOperation("删除工时记录")
    @DeleteMapping("/deleteWorkHourRecord")
    public ResponseResult deleteWorkHourRecord(@RequestParam(value = "id") Integer id){
        return workHourRecordService.deleteWorkHourRecord(id);
    }

    @ApiOperation("分页查询工时记录")
    @PostMapping("/listWorkHourRecord")
    public ResponseResult<IPage<SearchHourVO>> listWorkHourRecord(@RequestBody SearchHourDTO dto){
        Page<Object> page = new Page<>(dto.getPageNum(), dto.getPageSize());
        IPage<SearchHourVO> result = workHourRecordService.listWorkHourRecord(dto, page);
        return ResponseResult.okResult(result);
    }

}
