/*
package com.salary.management.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.salary.management.pojo.reward.dto.AddRewardDTO;
import com.salary.management.pojo.reward.dto.SearchRewardDTO;
import com.salary.management.pojo.reward.dto.UpdateRewardDTO;
import com.salary.management.pojo.reward.vo.SearchRewardVO;
import com.salary.management.service.RewardAndPunishmentService;
import com.salary.management.utils.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(tags = "奖惩记录接口")
@RestController
@RequestMapping("/rewardAndPunishment")
public class RewardAndPunishmentController {

    @Resource
    private RewardAndPunishmentService rewardAndPunishmentService;


    @ApiOperation("添加奖惩记录")
    @PostMapping("/addRewardAndPunishment")
    public ResponseResult addRewardAndPunishment(@RequestBody @Validated AddRewardDTO dto){
        return rewardAndPunishmentService.addRewardAndPunishment(dto);
    }

    @ApiOperation("删除奖惩记录")
    @DeleteMapping("/deleteRewardAndPunishment")
    public ResponseResult deleteRewardAndPunishment(Long id){
        return rewardAndPunishmentService.deleteRewardAndPunishment(id);
    }

    @ApiOperation("分页查询奖惩记录")
    @PostMapping("/listRewardAndPunishment")
    public ResponseResult<IPage<SearchRewardVO>> listRewardAndPunishment(@RequestBody SearchRewardDTO dto){
        Page<Object> page = new Page<>(dto.getPageNum(), dto.getPageSize());
        IPage<SearchRewardVO> result = rewardAndPunishmentService.listRewardAndPunishment(dto, page);
        return ResponseResult.okResult(result);
    }

    @ApiOperation("修改奖惩记录")
    @PostMapping("/updateRewardAndPunishment")
    public ResponseResult updateRewardAndPunishment(@RequestBody @Validated UpdateRewardDTO dto){
        return rewardAndPunishmentService.updateRewardAndPunishment(dto);
    }

}
*/
