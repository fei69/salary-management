package com.salary.management.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.salary.management.pojo.payroll.dto.CreatePayRollDTO;
import com.salary.management.pojo.payroll.dto.SearchPayRollDTO;
import com.salary.management.pojo.payroll.vo.SearchPayRollVO;
import com.salary.management.service.PayRollService;
import com.salary.management.utils.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(tags = "薪资单接口")
@RestController
@RequestMapping("/payRoll")
public class PayRollController {

    @Resource
    private PayRollService payRollService;

    @ApiOperation("生成薪资单")
    @PostMapping("/createPayRoll")
    public ResponseResult createPayRoll(@RequestBody @Validated CreatePayRollDTO dto){
        return payRollService.createPayRoll(dto);
    }

    @ApiOperation("分页查询薪资单")
    @PostMapping("/listPayRoll")
    public ResponseResult<IPage<SearchPayRollVO>> listPayRoll(@RequestBody SearchPayRollDTO dto){
        Page<Object> page = new Page<>(dto.getPageNum(), dto.getPageSize());
        IPage<SearchPayRollVO> result = payRollService.listPayRoll(dto, page);
        return ResponseResult.okResult(result);
    }
}
