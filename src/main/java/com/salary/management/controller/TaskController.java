package com.salary.management.controller;

import com.salary.management.service.TaskService;
import com.salary.management.utils.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(tags = "任务管理接口")
@RestController
@RequestMapping("/task")
public class TaskController {

    @Resource
    private TaskService taskService;

    @ApiOperation("计算员工月度绩效")
    @GetMapping("/countMonthPerformance")
    public ResponseResult countMonthPerformance(@RequestParam(value = "员工编号") Long staffId,
                                                @RequestParam(value = "日期(yyyy-MM)")String month){
        return taskService.countMonthPerformance(staffId,month);
    }

}
