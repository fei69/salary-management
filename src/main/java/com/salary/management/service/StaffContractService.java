package com.salary.management.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.salary.management.pojo.contract.dto.AddStaffContractDTO;
import com.salary.management.pojo.contract.dto.UpdateStaffContractDTO;
import com.salary.management.pojo.contract.entity.StaffContract;
import com.salary.management.pojo.contract.vo.SearchContractVO;
import com.salary.management.pojo.contract.dto.SearchContractDTO;
import com.salary.management.utils.ResponseResult;

public interface StaffContractService extends IService<StaffContract> {

    // 新增合同（备用，因为创建员工档案的同时也会创建对应的合同，只要进行修改操作来补充信息即可）
    ResponseResult addStaffContract(AddStaffContractDTO dto);

    // 修改合同
    ResponseResult updateStaffContract(UpdateStaffContractDTO dto);

    // 分页查询
    IPage<SearchContractVO> listStaffContract(SearchContractDTO dto, Page page);

    //根据id查询
    ResponseResult getStaffContractById(Long id);

    //根据id删除
    ResponseResult deleteStaffContract(Long id);


}
