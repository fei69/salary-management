package com.salary.management.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.salary.management.pojo.level.entity.SalaryLevel;

public interface SalaryLevelService extends IService<SalaryLevel> {
}
