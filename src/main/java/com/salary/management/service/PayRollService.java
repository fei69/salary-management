package com.salary.management.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.salary.management.pojo.payroll.dto.CreatePayRollDTO;
import com.salary.management.pojo.payroll.dto.SearchPayRollDTO;
import com.salary.management.pojo.payroll.entity.PayRoll;
import com.salary.management.pojo.payroll.vo.SearchPayRollVO;
import com.salary.management.utils.ResponseResult;

public interface PayRollService extends IService<PayRoll> {

    // 按月份生成薪资单
    ResponseResult createPayRoll(CreatePayRollDTO dto);

    IPage<SearchPayRollVO> listPayRoll(SearchPayRollDTO dto, Page page);
}
