package com.salary.management.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.salary.management.pojo.composition.dto.AddCompositionDTO;
import com.salary.management.pojo.composition.dto.SearchCompositionDTO;
import com.salary.management.pojo.composition.dto.UpdateCompositionDTO;
import com.salary.management.pojo.composition.entity.SalaryComposition;
import com.salary.management.pojo.composition.vo.SearchCompositionVO;
import com.salary.management.utils.ResponseResult;

public interface SalaryCompositionService extends IService<SalaryComposition> {

    //初始化员工薪资组成(备用，初始化优先在新增员工档案时完成)
    ResponseResult addSalaryComposition(AddCompositionDTO dto);

    // 修改员工薪资组成
    ResponseResult updateSalaryComposition(UpdateCompositionDTO dto);

    //分页查询员工薪资组成情况
    IPage<SearchCompositionVO> listSalaryComposition(SearchCompositionDTO dto, Page page);

    //删除薪资组成（备用，优先在删除员工档案时完成）
    ResponseResult deleteSalaryComposition(Long id);


}
