package com.salary.management.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.salary.management.pojo.file.dto.AddFileDTO;
import com.salary.management.pojo.file.dto.SearchFileDTO;
import com.salary.management.pojo.file.dto.UpdateFileDTO;
import com.salary.management.pojo.file.entity.StaffFile;
import com.salary.management.pojo.file.vo.SearchFileVO;
import com.salary.management.utils.ResponseResult;

import java.io.IOException;

public interface StaffFileService extends IService<StaffFile> {

    // 添加员工档案
    ResponseResult addStaffFile(AddFileDTO dto);

    // 分页查询员工档案
    IPage<SearchFileVO> listStaffFile(SearchFileDTO dto, Page page);

    // 修改员工档案
    ResponseResult updateStaffFile(UpdateFileDTO dto);

    // 删除员工档案
    ResponseResult deleteStaffFile(Long id);

    // 批量导出员工档案
    ResponseResult exportStaffFile() throws IOException;
}
