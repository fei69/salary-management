package com.salary.management.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.salary.management.pojo.user.dto.ForgetPasswordDTO;
import com.salary.management.pojo.user.dto.RegisterUserDTO;
import com.salary.management.pojo.user.entity.User;
import com.salary.management.utils.ResponseResult;

public interface LoginService extends IService<User> {

    //登录
    ResponseResult login(String username,String password);

    //修改密码
    ResponseResult updatePassword(Long id,String oldPassword,String newPassword);

    //退出登录
    ResponseResult logout();

    //注册
    ResponseResult register(RegisterUserDTO dto);

    //忘记密码
    ResponseResult forgetPassword(ForgetPasswordDTO dto);
}
