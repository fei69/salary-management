package com.salary.management.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.salary.management.pojo.task.entity.Task;
import com.salary.management.utils.ResponseResult;

import java.util.Map;

public interface TaskService extends IService<Task> {

    // 计算员工月度绩效
    ResponseResult countMonthPerformance(Long staffId,String month);
}
