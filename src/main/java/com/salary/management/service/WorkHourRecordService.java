package com.salary.management.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.salary.management.pojo.workhour.dto.AddWorkHourDTO;
import com.salary.management.pojo.workhour.dto.SearchHourDTO;
import com.salary.management.pojo.workhour.dto.UpdateWorkHourDTO;
import com.salary.management.pojo.workhour.entity.WorkHourRecord;
import com.salary.management.pojo.workhour.vo.SearchHourVO;
import com.salary.management.utils.ResponseResult;

import java.math.BigDecimal;
import java.util.Map;

public interface WorkHourRecordService extends IService<WorkHourRecord> {

    //记录工时
    ResponseResult addWorkHourRecord(AddWorkHourDTO dto);

    //修改工时
    ResponseResult updateWorkHourRecord(UpdateWorkHourDTO dto);

    //删除工时
    ResponseResult deleteWorkHourRecord(Integer id);

    //分页查询工时记录
    IPage<SearchHourVO> listWorkHourRecord(SearchHourDTO dto, Page page);

    // 计算加班补贴
    Map<String, BigDecimal> countOvertimeAllowance(Long staffId, String date);
}
