package com.salary.management.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.salary.management.mapper.RewardAndPunishmentMapper;
import com.salary.management.mapper.SalaryCompositionMapper;
import com.salary.management.mapper.WorkHourRecordMapper;
import com.salary.management.pojo.composition.vo.CompositionVO;
import com.salary.management.pojo.reward.entity.RewardAndPunishment;
import com.salary.management.pojo.workhour.dto.AddWorkHourDTO;
import com.salary.management.pojo.workhour.dto.SearchHourDTO;
import com.salary.management.pojo.workhour.dto.UpdateWorkHourDTO;
import com.salary.management.pojo.workhour.entity.WorkHourRecord;
import com.salary.management.pojo.workhour.vo.SearchHourVO;
import com.salary.management.service.WorkHourRecordService;
import com.salary.management.utils.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class WorkHourRecordServiceImpl extends ServiceImpl<WorkHourRecordMapper, WorkHourRecord> implements WorkHourRecordService {

    @Resource
    private WorkHourRecordMapper workHourRecordMapper;

    @Resource
    private RewardAndPunishmentMapper rewardAndPunishmentMapper;

    @Resource
    private SalaryCompositionMapper salaryCompositionMapper;

    @Override
    public ResponseResult addWorkHourRecord(AddWorkHourDTO dto) {
        // 正常工时不能超过8小时，加班工时不能超过3小时
        if("1".equals(dto.getHourType())){
            if(dto.getWorkHours() > 8){
                return ResponseResult.errorResult(AppHttpCodeEnum.WORK_HOUR_MUCH_THAN_NORMAL);
            }
        }else if("2".equals(dto.getHourType())){
            if(dto.getWorkHours() > 3){
                return ResponseResult.errorResult(AppHttpCodeEnum.ADD_HOUR_MUCH_THAN_NORMAL);
            }
        }
        // 同一日期不能添加两次相同类型的工时
        WorkHourRecord workHourRecord = BeanCopyUtils.copyBean(dto, WorkHourRecord.class);
        workHourRecordMapper.insert(workHourRecord);
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult updateWorkHourRecord(UpdateWorkHourDTO dto) {
        // 正常工时不能超过8小时，加班工时不能超过3小时
        if("1".equals(dto.getHourType())){
            if(dto.getWorkHours() > 8){
                return ResponseResult.errorResult(AppHttpCodeEnum.WORK_HOUR_MUCH_THAN_NORMAL);
            }
        }else if("2".equals(dto.getHourType())){
            if(dto.getWorkHours() > 3){
                return ResponseResult.errorResult(AppHttpCodeEnum.ADD_HOUR_MUCH_THAN_NORMAL);
            }
        }
        WorkHourRecord workHourRecord = BeanCopyUtils.copyBean(dto, WorkHourRecord.class);
        workHourRecordMapper.updateById(workHourRecord);
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult deleteWorkHourRecord(Integer id) {
        //TODO 角色权限 谁可以删 谁不能删
        workHourRecordMapper.deleteById(id);
        return ResponseResult.okResult();
    }

    @Override
    public IPage<SearchHourVO> listWorkHourRecord(SearchHourDTO dto, Page page) {
        List<SearchHourVO> listVO = workHourRecordMapper.listWorkHourRecord(dto, page);
        page.setRecords(listVO);
        return page;
    }

    @Override
    public Map<String,BigDecimal> countOvertimeAllowance(Long staffId, String date) {
        Map<String,BigDecimal> map = new HashMap<>();
        // 查询月度该员工所有的工时记录
        SearchHourDTO dto = new SearchHourDTO();
        dto.setStaffId(staffId);
        String beginTime = date +"-01";
        String endTime = DateUtils.nextMonth(date);
        dto.setBeginTime(beginTime);
        dto.setEndTime(endTime);
        List<SearchHourVO> list = listWorkHourRecord(dto, new Page()).getRecords();
        BigDecimal overtimeAllowance = new BigDecimal(0.00);
        BigDecimal leaveFine = new BigDecimal(0.00);
        BigDecimal askLeaveFine = new BigDecimal(0.00);
        CompositionVO baseComposition = salaryCompositionMapper.getBaseComposition(staffId);
        BigDecimal basicSalary = baseComposition.getBasicSalary();
        // 每日工资 / 默认一个月满勤20天
        BigDecimal dailySalary = BigDecimalUtil.divide(basicSalary, new BigDecimal(20), 2);
        for(SearchHourVO vo : list){
            // 超过8小时算作加班，加班一小时补贴20元
            if(vo.getWorkHours() > 8){
                Integer overtimeHour = vo.getWorkHours() - 8;
                BigDecimal bigDecimal = new BigDecimal(overtimeHour * 20);
                overtimeAllowance = BigDecimalUtil.add(overtimeAllowance,bigDecimal);
            }else if(0 <= vo.getWorkHours() && vo.getWorkHours() < 8){ // 少于8小时算作早退或缺勤，一次罚款50元
                leaveFine = BigDecimalUtil.add(leaveFine,new BigDecimal(50.00));
            }else if(vo.getAskLeaveFlag() == 1){
                askLeaveFine = BigDecimalUtil.add(askLeaveFine,dailySalary);
            }
        }
        map.put("overtimeAllowance",overtimeAllowance);
        map.put("leaveFine",leaveFine);
        map.put("askLeaveFine",askLeaveFine);
        /*if(BigDecimalUtil.isGreaterThanZero(overtimeAllowance)){
            RewardAndPunishment rewardAndPunishment = new RewardAndPunishment();
            rewardAndPunishment.setRewardAmount(overtimeAllowance);
            rewardAndPunishment.setRewardReason(date + "-月度加班补贴");
            rewardAndPunishment.setStaffId(staffId);
            rewardAndPunishment.setDate(new Date());
            rewardAndPunishmentMapper.insert(rewardAndPunishment);
        }else if(BigDecimalUtil.isGreaterThanZero(leaveFine)){
            RewardAndPunishment rewardAndPunishment = new RewardAndPunishment();
            rewardAndPunishment.setPunishAmount(leaveFine);
            rewardAndPunishment.setPunishReason(date + "-月度早退罚款");
            rewardAndPunishment.setStaffId(staffId);
            rewardAndPunishment.setDate(new Date());
            rewardAndPunishmentMapper.insert(rewardAndPunishment);
        }*/
        return map;
    }
}
