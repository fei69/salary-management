package com.salary.management.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.salary.management.mapper.SalaryCompositionMapper;
import com.salary.management.mapper.StaffFileMapper;
import com.salary.management.pojo.composition.dto.AddCompositionDTO;
import com.salary.management.pojo.composition.dto.SearchCompositionDTO;
import com.salary.management.pojo.composition.dto.UpdateCompositionDTO;
import com.salary.management.pojo.composition.entity.SalaryComposition;
import com.salary.management.pojo.composition.vo.SearchCompositionVO;
import com.salary.management.pojo.file.entity.StaffFile;
import com.salary.management.service.SalaryCompositionService;
import com.salary.management.utils.AppHttpCodeEnum;
import com.salary.management.utils.BeanCopyUtils;
import com.salary.management.utils.ResponseResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SalaryCompositionServiceImpl extends ServiceImpl<SalaryCompositionMapper, SalaryComposition> implements SalaryCompositionService {

    @Resource
    private SalaryCompositionMapper salaryCompositionMapper;

    @Resource
    private StaffFileMapper staffFileMapper;


    @Override
    public ResponseResult addSalaryComposition(AddCompositionDTO dto) {
        //TODO 最大最小期限校验
        SalaryComposition salaryComposition = BeanCopyUtils.copyBean(dto, SalaryComposition.class);
        salaryCompositionMapper.insert(salaryComposition);
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult updateSalaryComposition(UpdateCompositionDTO dto) {
        //TODO 最大最小期限校验
        SalaryComposition salaryComposition = BeanCopyUtils.copyBean(dto, SalaryComposition.class);
        salaryCompositionMapper.updateById(salaryComposition);
        return ResponseResult.okResult();
    }

    @Override
    public IPage<SearchCompositionVO> listSalaryComposition(SearchCompositionDTO dto, Page page) {
        List<SearchCompositionVO> listVO = salaryCompositionMapper.listSalaryComposition(dto, page);
        page.setRecords(listVO);
        return page;
    }

    @Override
    public ResponseResult deleteSalaryComposition(Long id) {
        // 在职状态判断，防止操作人员误操作
        StaffFile staffFileByParam = staffFileMapper.getStaffFileByParam(null, null, id);
        if(staffFileByParam.getStatus() == 0){
            return ResponseResult.errorResult(AppHttpCodeEnum.STAFF_IS_ACTIVE);
        }
        salaryCompositionMapper.deleteById(id);
        return ResponseResult.okResult();
    }
}
