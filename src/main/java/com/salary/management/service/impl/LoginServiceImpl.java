package com.salary.management.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.salary.management.mapper.UserMapper;
import com.salary.management.pojo.user.dto.ForgetPasswordDTO;
import com.salary.management.pojo.user.dto.RegisterUserDTO;
import com.salary.management.pojo.user.entity.User;
import com.salary.management.service.LoginService;
import com.salary.management.utils.AppHttpCodeEnum;
import com.salary.management.utils.BeanCopyUtils;
import com.salary.management.utils.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Service
public class LoginServiceImpl extends ServiceImpl<UserMapper, User> implements LoginService {

    @Resource
    private UserMapper userMapper;

    @Autowired
    private HttpServletRequest request;


    @Override
    public ResponseResult login(String username, String password) {
        if(!StringUtils.hasText(username)){
            return ResponseResult.errorResult(AppHttpCodeEnum.USERNAME_NULL);
        }
        if(!StringUtils.hasText(password)){
            return ResponseResult.errorResult(AppHttpCodeEnum.PASSWORD_NULL);
        }
        User user = userMapper.getAdminByParam(username, password,null);
        if(user != null){
            request.getSession().setAttribute("user", user);
            return ResponseResult.okResult(user);
        }else{
            return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_FAIL);
        }
    }

    @Override
    public ResponseResult updatePassword(Long id,String oldPassword, String newPassword) {
        //非空校验
        if(!StringUtils.hasText(oldPassword)){
            return ResponseResult.errorResult(AppHttpCodeEnum.OLD_PASSWORD_NULL);
        }
        if(!StringUtils.hasText(newPassword)){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEW_PASSWORD_NULL);
        }
        //重复性校验
        if(newPassword.equals(oldPassword)){
            return ResponseResult.errorResult(AppHttpCodeEnum.OLD_PASSWORD_EQUALS_NEW_PASSWORD);
        }
        //旧密码真实性校验，防止恶意修改
        User user = userMapper.getAdminByParam(null, oldPassword,null);
        if(user == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.OLD_PASSWORD_INCORRECT);
        }
        userMapper.updatePassword(id,newPassword);
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult logout() {
        request.getSession().removeAttribute("user");
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult register(RegisterUserDTO dto) {
        /*if(!StringUtils.hasText(dto.getUsername())){
            return ResponseResult.errorResult(AppHttpCodeEnum.USERNAME_NULL);
        }
        if(!StringUtils.hasText(dto.getPassword())){
            return ResponseResult.errorResult(AppHttpCodeEnum.PASSWORD_NULL);
        }*/
        User user = userMapper.getAdminByParam(dto.getUsername(), null,null);
        // 账号名不能和已有的重复
        if(user != null){
            return ResponseResult.errorResult(AppHttpCodeEnum.ACCOUNT_NAME_REPEAT);
        }
        //类型转换 dto -> entity
        User userToRegister = BeanCopyUtils.copyBean(dto, User.class);
        userMapper.insert(userToRegister);
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult forgetPassword(ForgetPasswordDTO dto) {
        // 验证手机号是否存在
        User user = userMapper.getAdminByParam(null, null, dto.getPhone());
        if(user == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PHONE_INCORRECT);
        }
        // 检查验证码是否正确
        String code = userMapper.getCodeByPhone(dto.getPhone());
        if(!code.equals(dto.getCode())){
            return ResponseResult.errorResult(AppHttpCodeEnum.CODE_INCORRECT);
        }
        return ResponseResult.okResult();
    }
}
