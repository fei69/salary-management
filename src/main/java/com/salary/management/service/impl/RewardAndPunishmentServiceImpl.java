package com.salary.management.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.salary.management.mapper.RewardAndPunishmentMapper;
import com.salary.management.mapper.StaffFileMapper;
import com.salary.management.pojo.file.entity.StaffFile;
import com.salary.management.pojo.reward.dto.AddRewardDTO;
import com.salary.management.pojo.reward.dto.SearchRewardDTO;
import com.salary.management.pojo.reward.dto.UpdateRewardDTO;
import com.salary.management.pojo.reward.entity.RewardAndPunishment;
import com.salary.management.pojo.reward.vo.SearchRewardVO;
import com.salary.management.service.RewardAndPunishmentService;
import com.salary.management.utils.AppHttpCodeEnum;
import com.salary.management.utils.BeanCopyUtils;
import com.salary.management.utils.ResponseResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

@Service
public class RewardAndPunishmentServiceImpl extends ServiceImpl<RewardAndPunishmentMapper, RewardAndPunishment> implements RewardAndPunishmentService {

    @Resource
    private RewardAndPunishmentMapper rewardAndPunishmentMapper;

    @Resource
    private StaffFileMapper staffFileMapper;

    @Override
    public ResponseResult addRewardAndPunishment(AddRewardDTO dto) {
        // 最大值不得高于1000
        BigDecimal max = new BigDecimal("1000.00");
        int i = max.compareTo(dto.getRewardAmount());
        int j = max.compareTo(dto.getPunishAmount());
        if(i < 0){
            return ResponseResult.errorResult(AppHttpCodeEnum.REWARD_AMOUNT_EXCEEDS_LIMIT);
        }
        if(j < 0){
            return ResponseResult.errorResult(AppHttpCodeEnum.PUNISHMENT_AMOUNT_EXCEEDS_LIMIT);
        }
        RewardAndPunishment rewardAndPunishment = BeanCopyUtils.copyBean(dto, RewardAndPunishment.class);
        rewardAndPunishmentMapper.insert(rewardAndPunishment);
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult deleteRewardAndPunishment(Long id) {
        rewardAndPunishmentMapper.deleteById(id);
        return ResponseResult.okResult();
    }

    @Override
    public IPage<SearchRewardVO> listRewardAndPunishment(SearchRewardDTO dto, Page page) {
        List<SearchRewardVO> listVO = rewardAndPunishmentMapper.listRewardAndPunishment(dto, page);
        for(SearchRewardVO vo : listVO){
            StaffFile staffFile = staffFileMapper.getStaffFileByParam(null, null, vo.getStaffId());
            if(staffFile.getName() != null){
                vo.setStaffName(staffFile.getName());
            }else{
                vo.setStaffName("姓名查询出错");
            }
        }
        page.setRecords(listVO);
        return page;
    }

    @Override
    public ResponseResult updateRewardAndPunishment(UpdateRewardDTO dto) {
        RewardAndPunishment rewardAndPunishment = BeanCopyUtils.copyBean(dto, RewardAndPunishment.class);
        rewardAndPunishmentMapper.updateById(rewardAndPunishment);
        return ResponseResult.okResult();
    }
}
