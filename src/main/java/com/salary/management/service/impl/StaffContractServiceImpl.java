package com.salary.management.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.salary.management.mapper.StaffContractMapper;
import com.salary.management.pojo.contract.dto.AddStaffContractDTO;
import com.salary.management.pojo.contract.dto.UpdateStaffContractDTO;
import com.salary.management.pojo.contract.entity.StaffContract;
import com.salary.management.pojo.contract.vo.SearchContractVO;
import com.salary.management.pojo.contract.dto.SearchContractDTO;
import com.salary.management.service.StaffContractService;
import com.salary.management.utils.AppHttpCodeEnum;
import com.salary.management.utils.BeanCopyUtils;
import com.salary.management.utils.ResponseResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class StaffContractServiceImpl extends ServiceImpl<StaffContractMapper, StaffContract> implements StaffContractService {

    @Resource
    private StaffContractMapper staffContractMapper;

    @Override
    public ResponseResult addStaffContract(AddStaffContractDTO dto) {
        // 重复性校验 身份证号
        if(staffContractMapper.getContractByParam(dto.getWorkerNumber(),null) != null){
            return ResponseResult.errorResult(AppHttpCodeEnum.WORKER_NUMBER_REPEAT);
        }
        StaffContract staffContract = BeanCopyUtils.copyBean(dto, StaffContract.class);
        staffContractMapper.insert(staffContract);
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult updateStaffContract(UpdateStaffContractDTO dto) {
        // 重复性校验 身份证号
        if(staffContractMapper.getContractByParam(dto.getWorkerNumber(),null) != null){
            return ResponseResult.errorResult(AppHttpCodeEnum.WORKER_NUMBER_REPEAT);
        }
        StaffContract staffContract = BeanCopyUtils.copyBean(dto, StaffContract.class);
        staffContractMapper.updateById(staffContract);
        return ResponseResult.okResult();
    }

    @Override
    public IPage<SearchContractVO> listStaffContract(SearchContractDTO dto, Page page) {
        List<SearchContractVO> listVO = staffContractMapper.listStaffContract(dto, page);
        page.setRecords(listVO);
        return page;
    }

    @Override
    public ResponseResult getStaffContractById(Long id) {
        StaffContract param = staffContractMapper.getContractByParam(null, id);
        return ResponseResult.okResult(param);
    }

    @Override
    public ResponseResult deleteStaffContract(Long id) {
        staffContractMapper.deleteById(id);
        return ResponseResult.okResult();
    }
}
