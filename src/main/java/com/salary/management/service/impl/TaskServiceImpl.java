package com.salary.management.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.salary.management.mapper.RewardAndPunishmentMapper;
import com.salary.management.mapper.TaskMapper;
import com.salary.management.pojo.reward.entity.RewardAndPunishment;
import com.salary.management.pojo.task.entity.Task;
import com.salary.management.pojo.task.vo.TaskVO;
import com.salary.management.service.TaskService;
import com.salary.management.utils.DateUtils;
import com.salary.management.utils.ResponseResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class TaskServiceImpl extends ServiceImpl<TaskMapper, Task> implements TaskService {

    @Resource
    private TaskMapper taskMapper;

    @Resource
    private RewardAndPunishmentMapper rewardAndPunishmentMapper;

    @Override
    public ResponseResult countMonthPerformance(Long staffId,String month) {
        // 找出该月份所有与员工有关的任务列表
        String beginDate = month + "-01";
        String endDate = DateUtils.nextMonth(month);
        List<TaskVO> list = taskMapper.listTaskByMonth(staffId, beginDate, endDate);
        Integer x = 0;
        for(TaskVO vo : list){
            String progress = vo.getCompletionProgress();
            String[] split = progress.split("%");
            //double y = Double.parseDouble(split[0]);
            int y = Integer.parseInt(split[0]);
            x = x + y;
        }
        Integer result = (x / list.size());
        // 绩效奖金
        BigDecimal performanceBonus = selectPerformanceAmount(result);
        /*// 添加奖惩记录
        RewardAndPunishment rewardAndPunishment = new RewardAndPunishment();
        rewardAndPunishment.setRewardAmount(performanceBonus);
        rewardAndPunishment.setRewardReason(month + "-月度绩效奖金");
        rewardAndPunishment.setStaffId(staffId);
        rewardAndPunishment.setDate(new Date());
        rewardAndPunishmentMapper.insert(rewardAndPunishment);*/
        return ResponseResult.okResult(performanceBonus);
    }

    public BigDecimal selectPerformanceAmount(Integer index){
        if(index == 100){
            return new BigDecimal(500);
        }else if(75 <= index && index < 100){
            return new BigDecimal(300);
        }else if(50 <= index && index < 75){
            return new BigDecimal(200);
        }else{
            return new BigDecimal(0.00);
        }
    }
}
