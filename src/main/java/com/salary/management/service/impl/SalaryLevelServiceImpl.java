package com.salary.management.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.salary.management.mapper.SalaryLevelMapper;
import com.salary.management.pojo.level.entity.SalaryLevel;
import com.salary.management.service.SalaryLevelService;
import org.springframework.stereotype.Service;

@Service
public class SalaryLevelServiceImpl extends ServiceImpl<SalaryLevelMapper, SalaryLevel> implements SalaryLevelService {
}
