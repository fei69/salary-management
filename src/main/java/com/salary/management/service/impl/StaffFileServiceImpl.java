package com.salary.management.service.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.style.column.LongestMatchColumnWidthStyleStrategy;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.salary.management.constant.ExcelConstants;
import com.salary.management.mapper.*;
import com.salary.management.pojo.composition.entity.SalaryComposition;
import com.salary.management.pojo.contract.entity.StaffContract;
import com.salary.management.pojo.excel.StaffFileExportExcelVO;
import com.salary.management.pojo.file.dto.AddFileDTO;
import com.salary.management.pojo.file.dto.SearchFileDTO;
import com.salary.management.pojo.file.dto.UpdateFileDTO;
import com.salary.management.pojo.file.entity.StaffFile;
import com.salary.management.pojo.file.vo.SearchFileVO;
import com.salary.management.pojo.payroll.entity.PayRoll;
import com.salary.management.pojo.reward.entity.RewardAndPunishment;
import com.salary.management.pojo.workhour.entity.WorkHourRecord;
import com.salary.management.utils.AppHttpCodeEnum;
import com.salary.management.utils.BeanCopyUtils;
import com.salary.management.utils.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import com.salary.management.service.StaffFileService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

@Slf4j
@Service
public class StaffFileServiceImpl extends ServiceImpl<StaffFileMapper, StaffFile> implements StaffFileService {

    @Resource
    private StaffFileMapper staffFileMapper;

    @Resource
    private StaffContractMapper staffContractMapper;

    @Resource
    private SalaryCompositionMapper salaryCompositionMapper;

    @Resource
    private HttpServletResponse httpServletResponse;

    @Override
    public ResponseResult addStaffFile(AddFileDTO dto) {
        // 重复性校验（身份证号、手机号）
        if(staffFileMapper.getStaffFileByParam(dto.getNumber(),null,null) != null){
            return ResponseResult.errorResult(AppHttpCodeEnum.NUMBER_REPEAT);
        }
        if(staffFileMapper.getStaffFileByParam(null,dto.getPhone(),null) != null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PHONE_REPEAT);
        }
        StaffFile staffFile = BeanCopyUtils.copyBean(dto, StaffFile.class);
        staffFile.setEntryTime(new Date());
        staffFileMapper.insert(staffFile);
        log.info("id:{}",staffFile.getId());
        //创建员工档案的同时，初始化薪资组成数据
        // 合同 - 弃用合同模块
        /*StaffContract staffContract = new StaffContract();
        staffContract.setStaffId(staffFile.getId());
        staffContract.setWorkerName(staffFile.getName());
        staffContractMapper.insert(staffContract);*/
        // 初始化薪资组成
        SalaryComposition salaryComposition = new SalaryComposition();
        salaryComposition.setStaffId(staffFile.getId());
        salaryCompositionMapper.insert(salaryComposition);
        // 更新外键
        //staffFile.setContractId(staffContract.getId());
        staffFile.setCompositionId(salaryComposition.getId());
        staffFileMapper.updateById(staffFile);
        return ResponseResult.okResult();
    }

    @Override
    public IPage<SearchFileVO> listStaffFile(SearchFileDTO dto, Page page) {
        List<SearchFileVO> listVO = staffFileMapper.listStaffFile(dto, page);
        page.setRecords(listVO);
        return page;
    }

    @Override
    public ResponseResult updateStaffFile(UpdateFileDTO dto) {
        // 重复性校验（排除本身）
        StaffFile paramByNumber = staffFileMapper.getStaffFileByParam(dto.getNumber(), null,null);
        if(paramByNumber != null && (paramByNumber.getId().longValue() != dto.getId().longValue())){
            return ResponseResult.errorResult(AppHttpCodeEnum.NUMBER_REPEAT);
        }
        StaffFile paramByPhone = staffFileMapper.getStaffFileByParam(null, dto.getPhone(),null);
        if(paramByPhone != null && (paramByPhone.getId().longValue() != dto.getId().longValue())){
            return ResponseResult.errorResult(AppHttpCodeEnum.PHONE_REPEAT);
        }
        StaffFile staffFile = BeanCopyUtils.copyBean(dto, StaffFile.class);
        staffFileMapper.updateById(staffFile);
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult deleteStaffFile(Long id) {
        // 在职状态无法删除
        StaffFile param = staffFileMapper.getStaffFileByParam(null, null, id);
        if(param.getStatus() != 1){
           return ResponseResult.errorResult(AppHttpCodeEnum.STAFF_IS_ACTIVE);
        }
        staffFileMapper.deleteById(id);
        // 薪资组成信息也一并删除
        Map<String, Object> map = new HashMap<>();
        map.put("staffId",id);
        SalaryComposition composition = salaryCompositionMapper.getCompositionByParam(map);
        salaryCompositionMapper.deleteById(composition.getId());
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult exportStaffFile() throws IOException {
        // 文件名
        String fileName = String.valueOf(System.currentTimeMillis());
        OutputStream outputStream = null;

        try{
            Long totalCount = staffFileMapper.selectCount(null);
            //每一个Sheet存放100w条数据
            Integer sheetDataRows = ExcelConstants.PER_SHEET_ROW_COUNT;
            //每次写入的数据量20w,每页查询20W
            Integer writeDataRows = ExcelConstants.PER_WRITE_ROW_COUNT;
            //计算需要的Sheet数量
            Long sheetNum = totalCount % sheetDataRows == 0 ? (totalCount / sheetDataRows) : (totalCount / sheetDataRows + 1);
            //计算一般情况下每一个Sheet需要写入的次数(一般情况不包含最后一个sheet,因为最后一个sheet不确定会写入多少条数据)
            Integer oneSheetWriteCount = sheetDataRows / writeDataRows;
            //计算最后一个sheet需要写入的次数
            Long lastSheetWriteCount = totalCount % sheetDataRows == 0 ? oneSheetWriteCount : (totalCount % sheetDataRows % writeDataRows == 0 ? (totalCount / sheetDataRows / writeDataRows) : ((totalCount % sheetDataRows) / writeDataRows + 1));
            outputStream = httpServletResponse.getOutputStream();
            //必须放到循环外，否则会刷新流
            ExcelWriter excelWriter = EasyExcel.write(outputStream).build();
            //开始分批查询分次写入
            for(int i = 0; i < sheetNum; i++){
                //创建Sheet
                WriteSheet sheet = new WriteSheet();
                sheet.setSheetName("Sheet"+i);
                sheet.setSheetNo(i);
                //循环写入次数: j的自增条件是当不是最后一个Sheet的时候写入次数为正常的每个Sheet写入的次数,如果是最后一个就需要使用计算的次数lastSheetWriteCount
                for (int j = 0; j < (i != sheetNum - 1 ? oneSheetWriteCount : lastSheetWriteCount); j++) {
                    //分页查询一次20w
                    Page page1 = new Page(j + 1 + oneSheetWriteCount * i, writeDataRows);
                    //查询分页列表---按照自己的业务查列表，分页这个一定要使用这个：page1.getPageNum(),page1.getPageSize()！！！
                    //List<Map<String, Object>> list = mapper.selectList(page1.getPageNum(),page1.getPageSize());
                    List<SearchFileVO> listVO = staffFileMapper.listStaffFile(null, null);
                    List<StaffFileExportExcelVO> SurfDayList = new ArrayList<>();
                    //写入到excel:
                    /**************z只需要选择一种方式即可*****************/
                    //这里可以通过设置includeColumnFiledNames、excludeColumnFiledNames导出什么字段，可以动态配置，前端传过来那些列，就导出那些列
       /*             //方式1、只导出v01301这一列
                    Set<String> includeColumnFiledNames = new HashSet<String>();
                    includeColumnFiledNames.add("v01301");
                    WriteSheet writeSheet = EasyExcel.writerSheet(i, "Sheet" + (i + 1)).head(AltitudeMonExportExcelVO.class).includeColumnFiledNames(includeColumnFiledNames)
                            .registerWriteHandler(new LongestMatchColumnWidthStyleStrategy()).build();
                    //方式2、排除v01301这一列，其他的全部导出
                    Set<String> excludeColumnFiledNames = new HashSet<String>();
                    excludeColumnFiledNames.add("v01301");
                    WriteSheet writeSheet = EasyExcel.writerSheet(i, "Sheet" + (i + 1)).head(AltitudeMonExportExcelVO.class).excludeColumnFiledNames(includeColumnFiledNames)
                            .registerWriteHandler(new LongestMatchColumnWidthStyleStrategy()).build();
                    excelWriter.write(list, writeSheet);*/
                    //方式3、不做设置，全部导出
                    WriteSheet writeSheet = EasyExcel.writerSheet(i, "Sheet" + (i + 1)).head(StaffFileExportExcelVO.class)
                            .registerWriteHandler(new LongestMatchColumnWidthStyleStrategy()).build();
                    excelWriter.write(listVO, writeSheet);
                }
            }
            // 下载excel
            httpServletResponse.setContentType("application/octet-stream");
            httpServletResponse.setCharacterEncoding("utf-8");
            httpServletResponse.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
            excelWriter.finish();
            outputStream.flush();
            outputStream.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
        return ResponseResult.okResult();
    }
}
