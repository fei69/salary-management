package com.salary.management.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.salary.management.mapper.PayRollMapper;
import com.salary.management.mapper.RewardAndPunishmentMapper;
import com.salary.management.mapper.SalaryCompositionMapper;
import com.salary.management.pojo.composition.vo.CompositionVO;
import com.salary.management.pojo.payroll.dto.CreatePayRollDTO;
import com.salary.management.pojo.payroll.dto.SearchPayRollDTO;
import com.salary.management.pojo.payroll.entity.PayRoll;
import com.salary.management.pojo.payroll.vo.SearchPayRollVO;
import com.salary.management.pojo.reward.dto.SearchRewardDTO;
import com.salary.management.pojo.reward.vo.SearchRewardVO;
import com.salary.management.service.PayRollService;
import com.salary.management.service.TaskService;
import com.salary.management.service.WorkHourRecordService;
import com.salary.management.utils.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PayRollServiceImpl extends ServiceImpl<PayRollMapper, PayRoll> implements PayRollService {

    @Resource
    private PayRollMapper payRollMapper;

    @Resource
    private SalaryCompositionMapper salaryCompositionMapper;

    @Resource
    private RewardAndPunishmentMapper rewardAndPunishmentMapper;

    @Resource
    private TaskService taskService;

    @Resource
    private WorkHourRecordService workHourRecordService;


    @Override
    public ResponseResult createPayRoll(CreatePayRollDTO dto) {
        // 重复性判断，防止二次生成相同薪资单
        Map<String,Object> param = new HashMap<>();
        param.put("date",dto.getGenerateDate());
        List<PayRoll> list = payRollMapper.listPayRoll(param,null);
        if(list.size() > 0){
            return ResponseResult.errorResult(AppHttpCodeEnum.PAYROLL_REPEAT);
        }
        // 应发工资表示基本工资+奖金+补贴
        // 从薪资组成表中获取员工的薪酬信息（基本工资、五险一金、补贴）
        CompositionVO baseComposition = salaryCompositionMapper.getBaseComposition(dto.getStaffId());
        PayRoll payRoll = BeanCopyUtils.copyBean(baseComposition, PayRoll.class);
        //BigDecimal netSalary = payRoll.getBasicSalary();
        BigDecimal payableSalary = payRoll.getBasicSalary();
        // 扣除五险一金、个人所得税之后，为应发工资
        payableSalary = BigDecimalUtil.subtract(payableSalary,payRoll.getEmploymentInjuryInsurance());
        payableSalary = BigDecimalUtil.subtract(payableSalary,payRoll.getEndownmentInsurance());
        payableSalary = BigDecimalUtil.subtract(payableSalary,payRoll.getIndividualIncomeTax());
        payableSalary = BigDecimalUtil.subtract(payableSalary,payRoll.getMaternityInsurance());
        payableSalary = BigDecimalUtil.subtract(payableSalary,payRoll.getMedicalInsurance());
        payableSalary = BigDecimalUtil.subtract(payableSalary,payRoll.getAccumulationFund());
        payableSalary = BigDecimalUtil.subtract(payableSalary,payRoll.getUnemploymentInsurance());
        payRoll.setPayableSalary(payableSalary);
        payRoll.setStaffId(dto.getStaffId());
        payRoll.setGenerateDate(dto.getGenerateDate());
        //接下来为实发工资 加上绩效、补贴、罚款等等
        // 计算绩效奖金
        BigDecimal performanceBonus = (BigDecimal) taskService.countMonthPerformance(dto.getStaffId(), dto.getGenerateDate()).getData();
        payableSalary = BigDecimalUtil.add(payableSalary,performanceBonus);
        payRoll.setPerformanceBonus(performanceBonus);
        // 加班补贴
        Map<String, BigDecimal> map = workHourRecordService.countOvertimeAllowance(dto.getStaffId(), dto.getGenerateDate());
        BigDecimal overtimeAllowance = map.get("overtimeAllowance");
        payRoll.setOvertimeAllowance(overtimeAllowance);
        payableSalary = BigDecimalUtil.add(payableSalary,overtimeAllowance);
        // 早退、缺勤扣款
        BigDecimal leaveFine = map.get("leaveFine");
        payRoll.setLeaveFine(leaveFine);
        payableSalary = BigDecimalUtil.subtract(payableSalary,leaveFine);
        // 请假扣款
        BigDecimal askLeaveFine = map.get("askLeaveFine");
        payableSalary = BigDecimalUtil.subtract(payableSalary,askLeaveFine);
        payRoll.setAskLeaveFine(askLeaveFine);

        // 加上交通补贴、午餐补贴
        payableSalary = BigDecimalUtil.add(payableSalary,payRoll.getLunchAllowance());
        payableSalary = BigDecimalUtil.add(payableSalary,payRoll.getTransportationAllowance());
        payRoll.setNetSalary(payableSalary);

        payRollMapper.insert(payRoll);
        return ResponseResult.okResult();
    }

    @Override
    public IPage<SearchPayRollVO> listPayRoll(SearchPayRollDTO dto, Page page) {
        Map<String,Object> param = new HashMap<>();
        param.put("staffId",dto.getStaffId());
        param.put("staffName",dto.getStaffName());
        param.put("generateDate",dto.getGenerateDate());
        List<PayRoll> list = payRollMapper.listPayRoll(param, page);
        page.setRecords(list);
        return page;
    }


}
