package com.salary.management.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.salary.management.pojo.reward.dto.AddRewardDTO;
import com.salary.management.pojo.reward.dto.SearchRewardDTO;
import com.salary.management.pojo.reward.dto.UpdateRewardDTO;
import com.salary.management.pojo.reward.entity.RewardAndPunishment;
import com.salary.management.pojo.reward.vo.SearchRewardVO;
import com.salary.management.utils.ResponseResult;

public interface RewardAndPunishmentService extends IService<RewardAndPunishment> {

    // 添加奖惩记录
    ResponseResult addRewardAndPunishment(AddRewardDTO dto);

    //删除奖惩记录
    ResponseResult deleteRewardAndPunishment(Long id);

    //分页查询
    IPage<SearchRewardVO> listRewardAndPunishment(SearchRewardDTO dto, Page page);

    //修改奖惩记录
    ResponseResult updateRewardAndPunishment(UpdateRewardDTO dto);

    // 计算绩效并添加奖项记录
    //ResponseResult addPerformanceReward();
}
