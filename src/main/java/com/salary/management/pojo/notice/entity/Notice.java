package com.salary.management.pojo.notice.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.salary.management.utils.BaseEntity;
import lombok.Data;

import java.util.Date;

@Data
public class Notice extends BaseEntity {

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    private Integer objectType;
    private Long objectId;
    private Date date;
    private Date expireTime;
    private String content;
}
