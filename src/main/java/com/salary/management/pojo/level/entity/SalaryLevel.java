package com.salary.management.pojo.level.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.salary.management.utils.BaseEntity;
import lombok.Data;

@Data
public class SalaryLevel extends BaseEntity {
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    private String startSalaryRange;
    private String endSalaryRange;
}
