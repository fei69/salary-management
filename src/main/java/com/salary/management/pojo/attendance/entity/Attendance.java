package com.salary.management.pojo.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.salary.management.utils.BaseEntity;
import lombok.Data;

import java.util.Date;

@Data
public class Attendance extends BaseEntity {

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    private Long staffId;
    private Integer requireAttendanceCount;
    private Integer lateCount;
    private Integer askLeaveCount;
    private Integer workOvertimeCount;
    private Integer actualAttendanceCount;
    private Integer leaveEarlyCount;
    private Integer absentCount;
    private Date date;
}
