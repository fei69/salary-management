package com.salary.management.pojo.file.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AddFileDTO {
    @ApiModelProperty("员工姓名")
    @NotBlank(message = "姓名不能为空")
    private String name;
    @ApiModelProperty("员工身份证号")
    @NotBlank(message = "身份证号不能为空")
    private String number;
    @ApiModelProperty("员工性别")
    @NotNull(message = "性别不能为空")
    private Integer sex;
    @ApiModelProperty("员工年龄")
    @NotNull(message = "年龄不能为空")
    private Integer age;
    @ApiModelProperty("员工出生日期")
    @NotBlank(message = "出生日期不能为空")
    private String birth;
    @ApiModelProperty("员工手机号")
    @NotBlank(message = "手机号不能为空")
    private String phone;
    @ApiModelProperty("员工住址")
    @NotBlank(message = "住址不能为空")
    private String address;
    @ApiModelProperty("员工邮箱")
    @NotBlank(message = "邮箱不能为空")
    private String email;
    @ApiModelProperty("员工学历")
    @NotNull(message = "学历不能为空")
    private Integer education;
    @ApiModelProperty("员工婚姻状况")
    @NotNull(message = "婚姻状况不能为空")
    private Integer marriageStatus;
    @ApiModelProperty("员工个人经历")
    @NotBlank(message = "个人经历不能为空")
    private String personalExperience;
    @ApiModelProperty("员工民族")
    @NotBlank(message = "民族不能为空")
    private String nation;
    @ApiModelProperty("员工所属省份")
    @NotNull(message = "省份不能为空")
    private Integer provinceId;
    @ApiModelProperty("员工所属城市")
    @NotNull(message = "城市不能为空")
    private Integer cityId;
    @ApiModelProperty("员工所属区域")
    @NotNull(message = "区域不能为空")
    private Integer areaId;
}
