package com.salary.management.pojo.file.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.salary.management.utils.BaseEntity;
import lombok.Data;

import java.util.Date;

@Data
public class StaffFile extends BaseEntity {

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    private String name;
    private String number;
    private Integer sex;
    private Integer age;
    private String birth;
    private String phone;
    private String address;
    private String email;
    private Integer education;
    private Integer marriageStatus;
    private String personalExperience;
    private String nation;
    private Long contractId;
    private Long hourId;
    private Long salaryLevelId;
    private Long rewardId;
    private Integer status;
    private Long salaryId;
    private Date entryTime;
    private Integer provinceId;
    private Integer cityId;
    private Integer areaId;
    private Long compositionId;
    private String contractUrl;
}
