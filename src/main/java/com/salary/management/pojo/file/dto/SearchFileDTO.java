package com.salary.management.pojo.file.dto;

import com.salary.management.utils.PageUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@ApiModel("SearchFileDTO")
@Data
public class SearchFileDTO extends PageUtil {

    @ApiModelProperty("编号")
    private Long id;
    @ApiModelProperty("姓名")
    private String name;
    @ApiModelProperty("身份证号")
    private String number;
    @ApiModelProperty("员工性别")
    private Integer sex;
    @ApiModelProperty("员工年龄")
    private Integer age;
    @ApiModelProperty("员工手机号")
    private String phone;
    @ApiModelProperty("员工邮箱")
    @NotBlank(message = "邮箱不能为空")
    private String email;

    //根据地区查找
    @ApiModelProperty("员工所属省份")
    private Integer provinceId;
    @ApiModelProperty("员工所属城市")
    private Integer cityId;
    @ApiModelProperty("员工所属区域")
    private Integer areaId;
    @ApiModelProperty("员工学历")
    private Integer education;
    @ApiModelProperty("员工个人经历")
    private Integer marriageStatus;
    @ApiModelProperty("员工民族")
    private String nation;

    // 根据入职时间计算
    @ApiModelProperty("开始时间")
    private String startTime;
    @ApiModelProperty("截止时间")
    private String endTime;

}
