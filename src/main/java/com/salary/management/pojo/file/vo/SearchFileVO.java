package com.salary.management.pojo.file.vo;

import lombok.Data;

import java.util.Date;

@Data
public class SearchFileVO {

    private Long id;
    private String name;
    private String number;
    private Integer sex;
    private Integer age;
    private String birth;
    private String phone;
    private String address;
    private String email;
    private Integer education;
    private Integer marriageStatus;
    private String personalExperience;
    private String nation;
    private Date entryTime;
    private Integer status;
}
