package com.salary.management.pojo.workhour.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class UpdateWorkHourDTO {

    @ApiModelProperty("id")
    @NotNull(message = "id不能为空")
    private Long id;
    @ApiModelProperty("员工档案编号")
    @NotNull(message = "员工档案编号不能为空")
    private Long staffId;
    @ApiModelProperty("工时")
    @NotNull(message = "工时不能为空")
    private Integer workHours;
    @ApiModelProperty("工时类型")
    @NotNull(message = "工时类型不能为空")
    private Integer hourType;
    @ApiModelProperty("记录日期")
    @NotBlank(message = "记录日期不能为空")
    private String date;
}
