package com.salary.management.pojo.workhour.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class SearchHourVO {

    @ApiModelProperty(name = "id",value = "")
    private Long id;
    @ApiModelProperty("员工档案编号")
    private Long staffId;
    @ApiModelProperty("工时")
    private Integer workHours;
    /*@ApiModelProperty(name = "工时类型",value = "")
    private Integer hourType;*/
    @ApiModelProperty("记录日期")
    private String date;
    @ApiModelProperty("请假标志")
    private Integer askLeaveFlag;
}
