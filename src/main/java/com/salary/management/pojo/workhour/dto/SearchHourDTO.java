package com.salary.management.pojo.workhour.dto;

import com.salary.management.utils.PageUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class SearchHourDTO extends PageUtil {

    @ApiModelProperty("员工档案编号")
    private Long staffId;
    @ApiModelProperty("工时范围起点(范围查询)")
    private Integer workHourBegin;
    @ApiModelProperty("工时范围终点(范围查询)")
    private Integer workHourEnd;
    /*@ApiModelProperty("工时类型")
    private Integer hourType;*/
    @ApiModelProperty("开始时间")
    private String beginTime;
    @ApiModelProperty("截止时间")
    private String endTime;
}
