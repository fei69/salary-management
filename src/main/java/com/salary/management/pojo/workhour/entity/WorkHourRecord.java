package com.salary.management.pojo.workhour.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.salary.management.utils.BaseEntity;
import lombok.Data;

@Data
public class WorkHourRecord extends BaseEntity {
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    private Long staffId;
    private Integer workHours;
    private String date;
    //private Integer hourType;
    private Integer askLeaveFlag;
}
