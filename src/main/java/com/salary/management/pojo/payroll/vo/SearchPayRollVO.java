package com.salary.management.pojo.payroll.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class SearchPayRollVO {

    private Long id;
    private Long staffId;
    @ApiModelProperty("员工姓名")
    private String staffName;
    @ApiModelProperty("基本工资")
    private BigDecimal basicSalary;
    /*@ApiModelProperty("奖金")
    private BigDecimal bonus;*/
    /*@ApiModelProperty("罚款")
    private BigDecimal fine;*/
    @ApiModelProperty("个人所得税")
    private BigDecimal individualIncomeTax;
    @ApiModelProperty("个人养老保险")
    private BigDecimal endownmentInsurance;
    @ApiModelProperty("个人医疗保险")
    private BigDecimal medicalInsurance;
    @ApiModelProperty("个人失业保险")
    private BigDecimal unemploymentInsurance;
    @ApiModelProperty("个人生育保险")
    private BigDecimal maternityInsurance;
    @ApiModelProperty("个人工伤保险")
    private BigDecimal employmentInjuryInsurance;
    @ApiModelProperty("个人公积金")
    private BigDecimal accumulationFund;
    @ApiModelProperty("交通补贴")
    private BigDecimal transportationAllowance;
    @ApiModelProperty("午餐补贴")
    private BigDecimal lunchAllowance;
    @ApiModelProperty("应发工资")
    private BigDecimal payableSalary;
    @ApiModelProperty("实发工资")
    private BigDecimal netSalary;
    @ApiModelProperty("生成日期（yyyy-MM）")
    private String generateDate;
    @ApiModelProperty("加班补贴")
    private BigDecimal overtimeAllowance;
    @ApiModelProperty("绩效奖金")
    private BigDecimal performanceBonus;
    @ApiModelProperty("早退、缺勤扣款")
    private BigDecimal leaveFine;
    @ApiModelProperty("请假扣款")
    private BigDecimal askLeaveFine;
}
