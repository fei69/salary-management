package com.salary.management.pojo.payroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.salary.management.utils.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class PayRoll extends BaseEntity {

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    private Long staffId;
    private Long contractId;
    private BigDecimal basicSalary;
    private BigDecimal bonus;
    private BigDecimal fine;
    private BigDecimal individualIncomeTax;
    private BigDecimal endownmentInsurance;
    private BigDecimal medicalInsurance;
    private BigDecimal unemploymentInsurance;
    private BigDecimal maternityInsurance;
    private BigDecimal employmentInjuryInsurance;
    private BigDecimal accumulationFund;
    private Long rewardId;
    private Long workHourId;
    private Long salaryLevelId;
    private Integer salaryLevel;
    private Date actualWorkTime;
    private BigDecimal payableSalary;
    private BigDecimal netSalary;
    private String generateDate;
    private Long compositionId;
    private BigDecimal transportationAllowance;
    private BigDecimal lunchAllowance;
    private BigDecimal overtimeAllowance;
    private BigDecimal performanceBonus;
    private BigDecimal leaveFine;
    private BigDecimal askLeaveFine;

}
