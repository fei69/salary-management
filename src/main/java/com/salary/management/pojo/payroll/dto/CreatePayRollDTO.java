package com.salary.management.pojo.payroll.dto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CreatePayRollDTO {

    @ApiModelProperty("员工编号")
    @NotNull(message = "员工编号不能为空")
    private Long staffId;
    @ApiModelProperty("员工姓名")
    @NotBlank(message = "员工姓名不能为空")
    private String staffName;
    @ApiModelProperty("生成日期")
    @NotBlank(message = "生成日期不能为空")
    private String generateDate;
}
