package com.salary.management.pojo.payroll.dto;

import com.salary.management.utils.PageUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SearchPayRollDTO extends PageUtil {

    //TODO  根据薪酬范围查询

    @ApiModelProperty("员工编号")
    private Long staffId;
    @ApiModelProperty("员工姓名")
    private String staffName;
    @ApiModelProperty("日期（yyyy-MM）")
    private String generateDate;
}
