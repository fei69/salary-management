package com.salary.management.pojo.task.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class TaskVO {

    private Long id;
    @ApiModelProperty("任务名称")
    private String taskName;
    @ApiModelProperty("指派人")
    private String assignBy;
    private Long assignById;
    @ApiModelProperty("处理人")
    private String processBy;
    private Long processById;
    @ApiModelProperty("完成进度（百分比）")
    private String completionProgress;
    @ApiModelProperty("指派时间")
    private Date assignTime;
    @ApiModelProperty("最后期限")
    private Date deadline;
}
