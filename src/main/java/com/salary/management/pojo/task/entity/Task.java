package com.salary.management.pojo.task.entity;

import com.salary.management.utils.BaseEntity;
import lombok.Data;

import java.util.Date;

@Data
public class Task extends BaseEntity {

    private Long id;
    private String taskName;
    private String assignBy;
    private Long assignById;
    private String processBy;
    private Long processById;
    private String completionProgress;
    private Date assignTime;
    private Date completionTime;
}
