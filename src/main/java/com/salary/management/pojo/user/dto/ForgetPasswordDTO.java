package com.salary.management.pojo.user.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ForgetPasswordDTO {
    @ApiModelProperty("验证码")
    @NotBlank(message = "验证码不能为空")
    private String code;
    @ApiModelProperty("手机号")
    @NotBlank(message = "手机号不能为空")
    private String phone;
}
