package com.salary.management.pojo.user.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class RegisterUserDTO {
    @ApiModelProperty("用户名")
    @NotBlank(message = "用户名不能为空")
    private String username;
    @ApiModelProperty("密码")
    @NotBlank(message = "密码不能为空")
    private String password;
    @ApiModelProperty("手机号")
    @NotBlank(message = "手机号不能为空")
    private String phone;
    @ApiModelProperty("验证码")
    @NotBlank(message = "验证码不能为空")
    private String code;
}
