package com.salary.management.pojo.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


@Data
public class StaffFileExportExcelVO {
    @ExcelProperty("姓名")
    private String name;
    @ExcelProperty("身份证号")
    private String number;
    @ExcelProperty("性别")
    private Integer sex;
    @ExcelProperty("年龄")
    private Integer age;
    @ExcelProperty("出生日期")
    private String birth;
    @ExcelProperty("电话")
    private String phone;
    @ExcelProperty("详细住址")
    private String address;
    @ExcelProperty("邮箱")
    private String email;
    @ExcelProperty("学历")
    private Integer education;
    @ExcelProperty("婚姻情况")
    private Integer marriageStatus;
    @ExcelProperty("个人经历")
    private String personalExperience;
    @ExcelProperty("民族")
    private String nation;
    @ExcelProperty("在职状态")
    private Integer status;
    @ExcelProperty("省份")
    private Integer provinceId;
    @ExcelProperty("城市")
    private Integer cityId;
    @ExcelProperty("区域")
    private Integer areaId;
}
