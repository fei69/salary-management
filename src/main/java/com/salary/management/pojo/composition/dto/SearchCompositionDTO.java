package com.salary.management.pojo.composition.dto;

import com.salary.management.utils.PageUtil;
import lombok.Data;

@Data
public class SearchCompositionDTO extends PageUtil {


    private String staffName;
}
