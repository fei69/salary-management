package com.salary.management.pojo.composition.entity;

import com.salary.management.utils.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class SalaryComposition extends BaseEntity {

    private Long id;
    private Long staffId;
    private BigDecimal individualIncomeTax;
    private BigDecimal endownmentInsurance;
    private BigDecimal medicalInsurance;
    private BigDecimal unemploymentInsurance;
    private BigDecimal maternityInsurance;
    private BigDecimal employmentInjuryInsurance;
    private BigDecimal accumulationFund;
    private BigDecimal transportationAllowance;
    private BigDecimal lunchAllowance;
    private String stockOption;
    private Long rewardId;
}
