package com.salary.management.pojo.composition.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CompositionVO {

    private BigDecimal individualIncomeTax;
    private BigDecimal endownmentInsurance;
    private BigDecimal medicalInsurance;
    private BigDecimal unemploymentInsurance;
    private BigDecimal maternityInsurance;
    private BigDecimal employmentInjuryInsurance;
    private BigDecimal accumulationFund;
    private BigDecimal transportationAllowance;
    private BigDecimal lunchAllowance;
    private BigDecimal basicSalary;
}
