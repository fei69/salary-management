package com.salary.management.pojo.composition.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class SearchCompositionVO {

    //TODO 薪资组成分页列表的查询条件都有哪些

    private Long id;
    private Long staffId;
    @ApiModelProperty("员工姓名")
    private String staffName;
    @ApiModelProperty("基本工资")
    private BigDecimal baseSalary;
    @ApiModelProperty("个人所得税")
    private BigDecimal individualIncomeTax;
    @ApiModelProperty("个人养老保险")
    private BigDecimal endownmentInsurance;
    @ApiModelProperty("个人医疗保险")
    private BigDecimal medicalInsurance;
    @ApiModelProperty("个人失业保险")
    private BigDecimal unemploymentInsurance;
    @ApiModelProperty("个人生育保险")
    private BigDecimal maternityInsurance;
    @ApiModelProperty("个人工伤保险")
    private BigDecimal employmentInjuryInsurance;
    @ApiModelProperty("个人公积金")
    private BigDecimal accumulationFund;
    @ApiModelProperty("交通补贴")
    private BigDecimal transportationAllowance;
    @ApiModelProperty("午餐补贴")
    private BigDecimal lunchAllowance;
    @ApiModelProperty("股票期权")
    private String stockOption;
    private Date updateTime;
    @ApiModelProperty("最后修改时间")
    private String showUpdateTime;
}
