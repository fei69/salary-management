package com.salary.management.pojo.composition.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class AddCompositionDTO {

    //TODO 这里留一个问题，正式员工和实习员工的薪资组成是否有区别，决定了是否需要非空判断

    @ApiModelProperty("员工档案外键")
    //@NotNull(message = "员工档案外键不能为空")
    private Long staffId;
    @ApiModelProperty("基本工资")
    private BigDecimal baseSalary;
    @ApiModelProperty("个人所得税")
    //@NotNull(message = "个人所得税不能为空")
    private BigDecimal individualIncomeTax;
    @ApiModelProperty("个人养老保险")
    //@NotNull(message = "个人养老保险不能为空")
    private BigDecimal endownmentInsurance;
    @ApiModelProperty("个人医疗保险")
    //@NotNull(message = "个人医疗保险不能为空")
    private BigDecimal medicalInsurance;
    @ApiModelProperty("个人失业保险")
    //@NotNull(message = "个人失业保险不能为空")
    private BigDecimal unemploymentInsurance;
    @ApiModelProperty("个人生育保险")
    //@NotNull(message = "个人生育保险不能为空")
    private BigDecimal maternityInsurance;
    @ApiModelProperty("个人工伤保险")
    //@NotNull(message = "个人工伤保险不能为空")
    private BigDecimal employmentInjuryInsurance;
    @ApiModelProperty("个人公积金")
    //@NotNull(message = "个人公积金不能为空")
    private BigDecimal accumulationFund;
    @ApiModelProperty("交通补贴")
    //@NotNull(message = "交通补贴不能为空")
    private BigDecimal transportationAllowance;
    @ApiModelProperty("午餐补贴")
    //@NotNull(message = "午餐补贴不能为空")
    private BigDecimal lunchAllowance;
    @ApiModelProperty("股票期权")
    //@NotBlank(message = "股票期权不能为空")
    private String stockOption;
}
