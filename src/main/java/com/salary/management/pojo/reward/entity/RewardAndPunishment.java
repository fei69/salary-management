package com.salary.management.pojo.reward.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.salary.management.utils.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class RewardAndPunishment extends BaseEntity {

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    private Long staffId;
    private String rewardReason;
    private BigDecimal rewardAmount;
    private String punishReason;
    private BigDecimal punishAmount;
    private Date date;
}
