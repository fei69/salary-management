package com.salary.management.pojo.reward.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class SearchRewardVO {

    @ApiModelProperty("id")
    private Long id;
    @ApiModelProperty("员工档案编号")
    private Long staffId;
    @ApiModelProperty("员工姓名")
    private String staffName;
    @ApiModelProperty("奖励原因")
    private String rewardReason;
    @ApiModelProperty("奖励金额")
    private BigDecimal rewardAmount;
    @ApiModelProperty("惩罚原因")
    private String punishReason;
    @ApiModelProperty("罚款金额")
    private BigDecimal punishAmount;

}
