package com.salary.management.pojo.reward.dto;

import com.salary.management.utils.PageUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class SearchRewardDTO extends PageUtil {

    @ApiModelProperty("员工档案编号")
    private Long staffId;
    @ApiModelProperty("奖励原因")
    private String rewardReason;
    @ApiModelProperty("奖励金额")
    private BigDecimal rewardAmount;
    @ApiModelProperty("惩罚原因")
    private String punishReason;
    @ApiModelProperty("罚款金额")
    private BigDecimal punishAmount;
    @ApiModelProperty("开始日期")
    private String beginDate;
    @ApiModelProperty("截止日期")
    private String endDate;
}
