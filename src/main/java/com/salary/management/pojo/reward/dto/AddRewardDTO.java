package com.salary.management.pojo.reward.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class AddRewardDTO {

    @ApiModelProperty("员工档案编号")
    @NotNull(message = "员工档案编号")
    private Long staffId;

    @ApiModelProperty("奖励原因")
    @NotBlank(message = "奖励原因")
    private String rewardReason;

    //@Min(value = 1000)
    @NotNull(message = "奖励金额")
    @ApiModelProperty("奖励金额")
    private BigDecimal rewardAmount;

    @NotBlank(message = "惩罚原因")
    @ApiModelProperty("惩罚原因")
    private String punishReason;

    //@Min(value = 1000)
    @ApiModelProperty("惩罚金额")
    @NotNull(message = "惩罚金额")
    private BigDecimal punishAmount;
}
