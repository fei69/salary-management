package com.salary.management.pojo.reward.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class UpdateRewardDTO {

    @ApiModelProperty("id")
    @NotNull(message = "id不能为空")
    private Long id;
    @ApiModelProperty("员工档案编号")
    @NotNull(message = "员工档案编号不能为空")
    private Long staffId;
    @ApiModelProperty("奖励原因")
    @NotBlank(message = "奖励原因不能为空")
    private String rewardReason;
    @ApiModelProperty("奖励金额")
    @NotNull(message = "奖励金额不能为空")
    private BigDecimal rewardAmount;
    @ApiModelProperty("惩罚原因")
    @NotBlank(message = "惩罚原因不能为空")
    private String punishReason;
    @ApiModelProperty("罚款金额")
    @NotNull(message = "罚款金额不能为空")
    private BigDecimal punishAmount;
}
