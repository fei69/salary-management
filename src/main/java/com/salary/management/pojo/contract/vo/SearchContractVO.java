package com.salary.management.pojo.contract.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class SearchContractVO {

    private Long id;
    private Long staffId;
    private String employerName;
    private String employerAddress;
    private String workerName;
    private String workerAddress;
    private String workerNumber;
    private Date startTime;
    private Date endTime;
    private BigDecimal remuneration;
}
