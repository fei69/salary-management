package com.salary.management.pojo.contract.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.salary.management.utils.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class StaffContract extends BaseEntity {

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    private Long staffId;
    private String employerName;
    private String employerAddress;
    private String employerLegalPerson;
    private String workerName;
    private String workerAddress;
    private String workerNumber;
    private Date startTime;
    private Date endTime;
    private BigDecimal remuneration;
}
