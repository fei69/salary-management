package com.salary.management.pojo.contract.dto;

import com.salary.management.utils.PageUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class SearchContractDTO extends PageUtil {

    @ApiModelProperty("用人单位名称")
    private String employerName;
    @ApiModelProperty("用人单位地址")
    private String employerAddress;
    @ApiModelProperty("用人单位法人")
    private String employerLegalPerson;
    @ApiModelProperty("劳动者名称")
    private String workerName;
    @ApiModelProperty("劳动者住址")
    private String workerAddress;
    @ApiModelProperty("劳动者身份证号")
    private String workerNumber;
    @ApiModelProperty("合同起始时间")
    private Date startTime;
    @ApiModelProperty("合同截止时间")
    private Date endTime;
    @ApiModelProperty("劳动报酬")
    private BigDecimal remuneration;
}
