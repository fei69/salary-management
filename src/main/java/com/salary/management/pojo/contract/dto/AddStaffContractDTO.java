package com.salary.management.pojo.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class AddStaffContractDTO {

    @ApiModelProperty("用人单位名称")
    @NotBlank(message = "用人单位名称不能为空")
    private String employerName;
    @ApiModelProperty("用人单位地址")
    @NotBlank(message = "用人单位地址不能为空")
    private String employerAddress;
    @ApiModelProperty("用人单位法人")
    @NotBlank(message = "用人单位法人不能为空")
    private String employerLegalPerson;
    @ApiModelProperty("劳动者名称")
    @NotBlank(message = "劳动者名称不能为空")
    private String workerName;
    @ApiModelProperty("劳动者住址")
    @NotBlank(message = "劳动者住址不能为空")
    private String workerAddress;
    @ApiModelProperty("劳动者身份证号")
    @NotBlank(message = "劳动者身份证号不能为空")
    private String workerNumber;
    @ApiModelProperty("合同起始日期")
    @NotNull(message = "合同起始不能为空")
    private Date startTime;
    @ApiModelProperty("合同截止日期")
    @NotNull(message = "合同截止不能为空")
    private Date endTime;
    @ApiModelProperty("劳动报酬")
    @NotNull(message = "劳动报酬不能为空")
    private BigDecimal remuneration;

}
