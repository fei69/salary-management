package com.salary.management.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.salary.management.pojo.user.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper extends BaseMapper<User> {

    User getAdminByParam(@Param("username") String username,
                         @Param("password") String password,
                         @Param("phone") String phone);

    void updatePassword(@Param("id") Long id,
                        @Param("password") String password);

    String getCodeByPhone(@Param("phone") String phone);

}
