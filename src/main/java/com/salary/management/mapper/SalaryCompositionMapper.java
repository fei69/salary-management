package com.salary.management.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.salary.management.pojo.composition.dto.SearchCompositionDTO;
import com.salary.management.pojo.composition.entity.SalaryComposition;
import com.salary.management.pojo.composition.vo.CompositionVO;
import com.salary.management.pojo.composition.vo.SearchCompositionVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SalaryCompositionMapper extends BaseMapper<SalaryComposition> {

    List<SearchCompositionVO> listSalaryComposition(@Param("param") SearchCompositionDTO dto,
                                                    Page page);

    // 根据多种条件查询单个对象
    SalaryComposition getCompositionByParam(@Param("paramMap") Map<String,Object> paramMap);

    CompositionVO getBaseComposition(@Param("staffId") Long staffId);
}
