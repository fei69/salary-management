package com.salary.management.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.salary.management.pojo.contract.entity.StaffContract;
import com.salary.management.pojo.contract.vo.SearchContractVO;
import com.salary.management.pojo.contract.dto.SearchContractDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface StaffContractMapper extends BaseMapper<StaffContract> {

    StaffContract getContractByParam(@Param("workerNumber") String workerNumber,
                                     @Param("id") Long id);

    List<SearchContractVO> listStaffContract(@Param("param") SearchContractDTO dto, Page page);


}
