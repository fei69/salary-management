package com.salary.management.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.salary.management.pojo.reward.dto.SearchRewardDTO;
import com.salary.management.pojo.reward.entity.RewardAndPunishment;
import com.salary.management.pojo.reward.vo.SearchRewardVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RewardAndPunishmentMapper extends BaseMapper<RewardAndPunishment> {

    List<SearchRewardVO> listRewardAndPunishment(@Param("param") SearchRewardDTO dto,
                                                 Page page);


}
