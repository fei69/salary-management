package com.salary.management.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.salary.management.pojo.task.entity.Task;
import com.salary.management.pojo.task.vo.TaskVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TaskMapper extends BaseMapper<Task> {

    List<TaskVO> listTaskByMonth(@Param("staffId") Long staffId,
                                 @Param("beginDate") String beginDate,
                                 @Param("endDate") String endDate);
}
