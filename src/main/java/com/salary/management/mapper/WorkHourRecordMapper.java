package com.salary.management.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.salary.management.pojo.workhour.dto.SearchHourDTO;
import com.salary.management.pojo.workhour.entity.WorkHourRecord;
import com.salary.management.pojo.workhour.vo.SearchHourVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface WorkHourRecordMapper extends BaseMapper<WorkHourRecord> {

    List<SearchHourVO> listWorkHourRecord(@Param("param") SearchHourDTO dto,
                                          Page page);
}
