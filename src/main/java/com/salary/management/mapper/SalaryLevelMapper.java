package com.salary.management.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.salary.management.pojo.level.entity.SalaryLevel;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SalaryLevelMapper extends BaseMapper<SalaryLevel> {
}
