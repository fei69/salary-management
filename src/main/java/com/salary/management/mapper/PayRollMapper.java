package com.salary.management.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.salary.management.pojo.payroll.entity.PayRoll;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PayRollMapper extends BaseMapper<PayRoll> {

    List<PayRoll> listPayRoll(@Param("param") Map<String,Object> map,
                              Page page);
}
