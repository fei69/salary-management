package com.salary.management.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.salary.management.pojo.file.dto.SearchFileDTO;
import com.salary.management.pojo.file.entity.StaffFile;
import com.salary.management.pojo.file.vo.SearchFileVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface StaffFileMapper extends BaseMapper<StaffFile> {

    StaffFile getStaffFileByParam(@Param("number") String number,
                                  @Param("phone") String phone,
                                  @Param("id") Long id);

    List<SearchFileVO> listStaffFile(@Param("param") SearchFileDTO dto,
                                     Page page);



}
