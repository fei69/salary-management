package com.salary.management.utils;

import java.math.BigDecimal;


public class BigDecimalUtil {
    /**
     * 加法计算（result = x + y）
     *
     * @param x 被加数（可为null）
     * @param y 加数 （可为null）
     * @return 和 （可为null）
     * @author dengcs
     */
    public static BigDecimal add(BigDecimal x, BigDecimal y) {
        if (x == null) {
            return y;
        }
        if (y == null) {
            return x;
        }
        return x.add(y);
    }

    /**
     * 加法计算（result = a + b + c + d）
     *
     * @param a 被加数（可为null）
     * @param b 加数（可为null）
     * @param c 加数（可为null）
     * @param d 加数（可为null）
     * @return BigDecimal （可为null）
     * @author dengcs
     */
    public static BigDecimal add(BigDecimal a, BigDecimal b, BigDecimal c, BigDecimal d) {
        BigDecimal ab = add(a, b);
        BigDecimal cd = add(c, d);
        return add(ab, cd);
    }

    /**
     * 累加计算(result=x + result)
     *
     * @param x      被加数（可为null）
     * @param result 和 （可为null,若被加数不为为null，result默认值为0）
     * @return result 和 （可为null）
     * @author dengcs
     */
    public static BigDecimal accumulate(BigDecimal x, BigDecimal result) {
        if (x == null) {
            return result;
        }
        if (result == null) {
            result = new BigDecimal("0");
        }
        return result.add(x);
    }

    /**
     * 减法计算(result = x - y)
     *
     * @param x 被减数（可为null）
     * @param y 减数（可为null）
     * @return BigDecimal 差 （可为null）
     * @author dengcs
     */
    public static BigDecimal subtract(BigDecimal x, BigDecimal y) {
        if (x == null || y == null) {
            return null;
        }
        return x.subtract(y);
    }

    public static BigDecimal subtracts(BigDecimal i, BigDecimal... arg) {
        BigDecimal difference = i;
        for (BigDecimal b : arg) {
            difference = difference.subtract(b);
        }
        return difference;
    }

    /**
     * 乘法计算(result = x × y)
     *
     * @param x 乘数(可为null)
     * @param y 乘数(可为null)
     * @return BigDecimal 积
     * @author dengcs
     */
    public static BigDecimal multiply(BigDecimal x, BigDecimal y) {
        if (x == null || y == null) {
            return null;
        }
        return x.multiply(y);
    }

    /**
     * 乘法计算(result = x × y)
     *
     * @param x     乘数(可为null)
     * @param y     乘数(可为null)
     * @param scale 小数位 （可为null,四舍五入，默认保留2位小数）
     * @return
     */
    public static BigDecimal multiply(BigDecimal x, BigDecimal y, Integer scale) {
        if (x == null || y == null) {
            return null;
        }
        scale = scale == null ? 2 : scale;
        return x.multiply(y).setScale(scale, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 除法计算(result = x ÷ y)
     *
     * @param x     被除数（可为null）
     * @param y     除数（可为null）
     * @param scale 保留小数位（可为null，默认20）
     * @return 商 （可为null,四舍五入，默认保留20位小数）
     * @author dengcs
     */
    public static BigDecimal divide(BigDecimal x, BigDecimal y, Integer scale) {
        if (x == null || y == null || y.compareTo(BigDecimal.ZERO) == 0) {
            return null;
        }
        scale = scale == null ? 20 : scale;
        // 结果为0.000..时，不用科学计数法展示
        return stripTrailingZeros(x.divide(y, scale, BigDecimal.ROUND_HALF_UP));
    }

    /**
     * 除法计算(result = x ÷ y)
     *
     * @param x     被除数（可为null）
     * @param y     除数（可为null）
     * @param scale 保留小数位（可为null，默认20）
     * @param roundingMode
     * @return 商 （可为null,四舍五入，默认保留20位小数）
     */
    public static BigDecimal divide(BigDecimal x, BigDecimal y, Integer scale, Integer roundingMode) {
        if (x == null || y == null || y.compareTo(BigDecimal.ZERO) == 0) {
            return null;
        }
        scale = scale == null ? 20 : scale;
        roundingMode = roundingMode == null ? BigDecimal.ROUND_HALF_UP : roundingMode;
        // 结果为0.000..时，不用科学计数法展示
        return stripTrailingZeros(x.divide(y, scale, roundingMode));
    }

    /**
     * 转为字符串(防止返回可续计数法表达式)
     *
     * @param x 要转字符串的小数
     * @return String
     * @author dengcs
     */
    public static String toPlainString(BigDecimal x) {
        if (x == null) {
            return null;
        }
        return x.toPlainString();
    }

    /**
     * 保留小数位数
     *
     * @param x     目标小数
     * @param scale 要保留小数位数
     * @return BigDecimal 结果四舍五入
     * @author dengcs
     */
    public static BigDecimal scale(BigDecimal x, int scale) {
        if (x == null) {
            return null;
        }
        return x.setScale(scale, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 整型转为BigDecimal
     *
     * @param x(可为null)
     * @return BigDecimal (可为null)
     * @author dengcs
     */
    public static BigDecimal toBigDecimal(Integer x) {
        if (x == null) {
            return null;
        }
        return new BigDecimal(x.toString());
    }

    /**
     * 长整型转为BigDecimal
     *
     * @param x(可为null)
     * @return BigDecimal (可为null)
     * @author dengcs
     */
    public static BigDecimal toBigDecimal(Long x) {
        if (x == null) {
            return null;
        }
        return new BigDecimal(x.toString());
    }

    /**
     * 双精度型转为BigDecimal
     *
     * @param x(可为null)
     * @return BigDecimal (可为null)
     * @author dengcs
     */
    public static BigDecimal toBigDecimal(Double x) {
        if (x == null) {
            return null;
        }
        return new BigDecimal(Double.toString(x));
    }

    /**
     * 单精度型转为BigDecimal
     *
     * @param x(可为null)
     * @return BigDecimal (可为null)
     * @author dengcs
     */
    public static BigDecimal toBigDecimal(Float x) {
        if (x == null) {
            return null;
        }
        return new BigDecimal(x.toString());
    }

    /**
     * 字符串型转为BigDecimal
     *
     * @param x(可为null)
     * @return BigDecimal (可为null)
     * @author dengcs
     */
    public static BigDecimal toBigDecimal(String x) {
        if (x == null) {
            return null;
        }
        return new BigDecimal(x);
    }

    /**
     * 对象类型转为BigDecimal
     *
     * @param x(可为null)
     * @return BigDecimal (可为null)
     * @author dengcs
     */
    public static BigDecimal toBigDecimal(Object x) {
        if (x == null) {
            return null;
        }
        BigDecimal result = null;
        try {
            result = new BigDecimal(x.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 倍数计算，用于单位换算
     *
     * @param x        目标数(可为null)
     * @param multiple 倍数 (可为null)
     * @return BigDecimal (可为null)
     * @author dengcs
     */
    public static BigDecimal multiple(BigDecimal x, Integer multiple) {
        if (x == null || multiple == null) {
            return null;
        }
        return BigDecimalUtil.multiply(x, toBigDecimal(multiple));
    }

    /**
     * 去除小数点后的0（如: 输入1.000返回1）
     *
     * @param x 目标数(可为null)
     * @return
     */
    public static BigDecimal stripTrailingZeros(BigDecimal x) {
        if (x == null) {
            return null;
        }
        return x.stripTrailingZeros();
    }

    /**
     * 是否大于0
     *
     * @param value
     * @return
     */
    public static boolean isGreaterThanZero(BigDecimal value) {
        if (null == value) {
            value = BigDecimal.ZERO;
        }
        return value.compareTo(BigDecimal.ZERO) > 0;
    }

    /**
     * 大于等于0
     *
     * @param value
     * @return
     */
    public static boolean isGreaterThanOrEqualToZero(BigDecimal value) {
        if (null == value) {
            value = BigDecimal.ZERO;
        }
        return value.compareTo(BigDecimal.ZERO) >= 0;
    }

    /**
     * 是否小于0
     *
     * @param value
     * @return
     */
    public static boolean isLessThanZero(BigDecimal value) {
        if (null == value) {
            value = BigDecimal.ZERO;
        }
        return value.compareTo(BigDecimal.ZERO) < 0;
    }

    /**
     * 是否小于等于0
     *
     * @param value
     * @return
     */
    public static boolean isLessThanOrEqualToZero(BigDecimal value) {
        if (null == value) {
            value = BigDecimal.ZERO;
        }
        return value.compareTo(BigDecimal.ZERO) <= 0;
    }

    /**
     * 是否大于
     *
     * @param v1
     * @param v2
     * @return
     */
    public static boolean isGreaterThan(BigDecimal v1, BigDecimal v2) {
        if (null == v1) {
            v1 = BigDecimal.ZERO;
        }
        if (null == v2) {
            v2 = BigDecimal.ZERO;
        }
        return v1.compareTo(v2) > 0;
    }

    /**
     * 是否大于等于
     *
     * @param v1
     * @param v2
     * @return
     */
    public static boolean isGreaterThanOrEqualTo(BigDecimal v1, BigDecimal v2) {
        if (null == v1) {
            v1 = BigDecimal.ZERO;
        }
        if (null == v2) {
            v2 = BigDecimal.ZERO;
        }
        return v1.compareTo(v2) >= 0;
    }

    /**
     * 是否小于
     *
     * @param v1
     * @param v2
     * @return
     */
    public static boolean isLessThan(BigDecimal v1, BigDecimal v2) {
        if (null == v1) {
            v1 = BigDecimal.ZERO;
        }
        if (null == v2) {
            v2 = BigDecimal.ZERO;
        }
        return v1.compareTo(v2) < 0;
    }

    /**
     * 是否小于等于
     *
     * @param v1
     * @param v2
     * @return
     */
    public static boolean isLessThanOrEqualTo(BigDecimal v1, BigDecimal v2) {
        if (null == v1) {
            v1 = BigDecimal.ZERO;
        }
        if (null == v2) {
            v2 = BigDecimal.ZERO;
        }
        return v1.compareTo(v2) <= 0;
    }

    /**
     * 是否等于
     *
     * @param v1
     * @param v2
     * @return
     */
    public static boolean isEqualTo(BigDecimal v1, BigDecimal v2) {
        return v1.compareTo(v2) == 0;
    }

    /**
     * 是否是0
     *
     * @param v1
     * @return
     */
    public static boolean isZero(BigDecimal v1) {
        return v1.compareTo(BigDecimal.ZERO) == 0;
    }
}

