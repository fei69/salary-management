package com.salary.management.utils;

public enum AppHttpCodeEnum {

    //成功状态
    SUCCESS(200,"success"),

    // 员工模块
    STAFF_NAME_NULL(501,"员工名称为空"),
    STAFF_NUMBER_NULL(501,"员工身份证号为空"),
    STAFF_SEX_NULL(501,"员工性别为空"),
    STAFF_AGE_NULL(501,"员工年龄为空"),
    STAFF_PHONE_NULL(501,"员工电话为空"),
    STAFF_ADDRESS_NULL(501,"员工住址为空"),
    STAFF_POSITION_NULL(501,"员工职位为空"),
    STAFF_EDUCATION_NULL(501,"员工学历为空"),
    DEPARTMENT_NULL(501,"员工所在部门为空"),
    STAFF_SALARY_NULL(501,"员工薪资为空"),
    //STAFF_PHONE_OR_NUMBER_REPEAT(502,"身份证号或电话不能重复"),
    NUMBER_REPEAT(502,"请正确填写您的身份证号"),
    PHONE_REPEAT(502,"请正确填写您的手机号"),
    STAFF_IS_ACTIVE(503,"员工非离职状态，不可删除"),
    STAFF_ID_EMPTY(504,"ID为空"),

    //薪资单模块
    PAYROLL_REPEAT(1200,"该记录日期已存在薪资单，请检查是否重复操作"),

    //合同模块
    WORKER_NUMBER_REPEAT(600,"请正确输入劳动者的身份证号"),

    // 薪资组成模块

    //奖惩记录模块
    REWARD_AMOUNT_EXCEEDS_LIMIT(701,"奖金大于最大值"),
    PUNISHMENT_AMOUNT_EXCEEDS_LIMIT(702,"罚款大于最大值"),


    //工时模块
    WORK_HOUR_MUCH_THAN_NORMAL(1001,"正常工时不能超过8小时"),
    ADD_HOUR_MUCH_THAN_NORMAL(1002,"加班工时不能超过3小时"),

    //参数模块
    SEARCH_CONDITIONS_NULL(301,"分页参数或查询参数不能为空"),

    //账号模块
    OLD_PASSWORD_INCORRECT(901,"旧密码不正确！"),
    OLD_PASSWORD_EQUALS_NEW_PASSWORD(902,"新旧密码不能相同！"),
    OLD_PASSWORD_NULL(904,"旧密码为空"),
    NEW_PASSWORD_NULL(905,"新密码为空"),
    ACCOUNT_NAME_REPEAT(903,"账号名不能重复！"),
    PHONE_INCORRECT(906,"手机号不正确!"),
    CODE_INCORRECT(907,"验证码错误!"),

    //薪资明细模块
    SUM_INSURANCE_LESS_THAN_LOWER_STANDARD(1200,"五险一金不能低于最低标准（3500）!"),

    //登录模块
    LOGIN_SUCCESS(200,"登录成功!"),
    USERNAME_NULL(801,"账号为空！"),
    PASSWORD_NULL(802,"密码为空！"),
    LOGIN_FAIL(803,"登录失败，请检查账号密码是否正确！"),

    //操作模块
    DELETE_FAIL(201,"删除失败"),
    UPDATE_FAIL(201,"更新失败"),
    INSERT_FAIL(201,"插入失败");



    int code;
    String msg;

    AppHttpCodeEnum(int code, String errorMessage){
        this.code = code;
        this.msg = errorMessage;
    }

    public int getCode(){
        return code;
    }

    public String getMsg(){
        return msg;
    }
}
