package com.salary.management.utils;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PageUtil {
    @NotNull(message = "页码不能为空")
    @ApiModelProperty(value = "页码", required = false)
    private Integer pageNum;
    @NotNull(message = "页数不能为空")
    @ApiModelProperty(value = "页数", required = false)
    private Integer pageSize;


    public PageUtil() {

    }

    public PageUtil(Integer pageNum, Integer pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }
}
