package com.salary.management.utils;

import org.springframework.stereotype.Component;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Component
public class WebUtils {

    /**
     * 将字符串渲染到客户端
     * @param response
     * @param string
     */
    public static void renderString(HttpServletResponse response,String string){
        try{
            response.setStatus(200);
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            response.getWriter().print(string);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void setDownLoadHeader(String filename, ServletContext context,HttpServletResponse response) throws UnsupportedEncodingException {
        //获取文件的mime类型
        String mimeType = context.getMimeType(filename);
        response.setHeader("content-type",mimeType);
        String fname = URLEncoder.encode(filename,"UTF-8");
        response.setHeader("Content-disposition","attachment;filename="+filename);
    }

    public static void setDownLoadHeader(String filename,HttpServletResponse response) throws UnsupportedEncodingException{
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        String fname = URLEncoder.encode(filename,"UTF-8").replaceAll("\\+","%20");
        response.setHeader("Content-disposition","attachment;filename="+filename);
    }
}
