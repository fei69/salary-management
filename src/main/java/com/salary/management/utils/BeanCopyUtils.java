package com.salary.management.utils;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class BeanCopyUtils {

    private BeanCopyUtils(){

    }

    /**
     * 单个对象的拷贝
     * @param source
     * @param clazz
     * @param <V>
     * @return
     */
    public static <V> V copyBean(Object source,Class<V> clazz){
        //利用反射创建接收对象
        V result = null;
        try{
            result = clazz.newInstance();
            //实现属性copy
            BeanUtils.copyProperties(source,result);
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 列表对象的拷贝
     * @param list
     * @param clazz
     * @param <O>
     * @param <V>
     * @return
     */
    public static <O,V> List<V> copyBeanList(List<O> list, Class<V> clazz){
        return list.stream()
                //转换对象，将每个对象都进行拷贝
                .map(o -> copyBean(o,clazz))
                .collect(Collectors.toList());
    }


}
