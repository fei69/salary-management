package com.salary.management.utils;

public class DateUtils {

    public static boolean judgeMonthlyOrBimonthly(String month){
        if("2".equals(month) || "4".equals(month) || "6".equals(month) || "9".equals(month) || "11".equals(month)){
            return false;
        }
        return true;
    }

    public static String nextMonth(String month){
        String[] split = month.split("-");
        Integer indexMonth = Integer.parseInt(split[1]);
        Integer indexYear = Integer.parseInt(split[0]);
        // 若为12月，则下个月份是明年的1月份
        if(indexMonth != 12){
            indexMonth ++;
        }else{
            indexYear ++ ;
            indexMonth = 1;
        }
        return indexYear + "-" + indexMonth + "-01";
    }
}
